function [transform_params, residual, current_inliers ] = RANSAC( pts_src, pts_tgt, min_residual, transformObj, matched, ntrials_ )
%RANSAC Summary of this function goes here
%   Detailed explanation goes here

% Pick some points and calculate the transform from points
params = transformObj.p0;
N = size(pts_src,2);
n = size(matched,1);

ntrials = n*ntrials_;
npts = 3;

current_max_inliers = 0;
current_inliers = [];
for i=1:ntrials
    sidx = NaN(npts,1);
    sidx(1) = ceil(rand(1,1)*n);
    
    for j=2:npts
        newidx = sidx(j-1);
        while nnz(~isnan(setdiff(sidx,newidx)))<j-1
            newidx = ceil(rand(1,1)*n);
        end
        sidx(j) = newidx;
    end
    % --------------------------------
    ps.points = pts_src(matched(sidx,1),:);
    pt.points = pts_tgt(matched(sidx,2),:);
    
    [T, residual] = transformObj.calculateFromPointSets(ps,pt, (1:npts)');
    if residual>min_residual
        continue;
    end
    % --------------------------------
    % Compute how many points have low residual
    M = eye(N+1);
    M(1:N,1:N) = T.rot;
    M(1:N,N+1) = T.translation;
    
    ps_warped = transformObj.transform_function(pts_src(matched(:,1),:),M);
    dif = pts_tgt(matched(:,2),:) - ps_warped;
    residual = sqrt(sum(dif.^2,2));
    
    inliers = find(residual<min_residual);
    
    if numel(inliers)> current_max_inliers
        current_max_inliers  = numel(inliers);
        current_inliers = inliers;
    end
end

% from the inliers, calculate the transform
ps.points = pts_src(matched(current_inliers,1),:);
pt.points = pts_tgt(matched(current_inliers,2),:);
    
[T, residual] = transformObj.calculateFromPointSets(ps,pt, (1:numel(current_inliers))');

M = eye(N+1);
M(1:N,1:N) = T.rot;
M(1:N,N+1) = T.translation;
transform_params = M;



end

