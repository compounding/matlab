function [img, orig]=read_picture(filename)
% 

img_ = imread(filename);

img = ImageType([size(img_,2) size(img_,1)]', [0 0]', [1 1]');
img.data = double(flipud(rgb2gray(img_))');
orig1 = double(flipud(img_(:,:,1))');
orig2 = double(flipud(img_(:,:,2))');
orig3 = double(flipud(img_(:,:,3))');
orig = cat(3,orig1, orig2, orig3);


end