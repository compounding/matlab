function write_deformetricaVTKShape(filename, input)
% Function for writing a shape for use in deformetrica, which uses the
% Visualization Toolkit (VTK) legacy format.
%
% write_deformetricaVTKShape(filename,input);
%
%   input is a struct with the following fields
%   input.x     a Nx2 or Nx3 array with the node positions
%   input.d     (optional) a Nx2 or Nx3 array with the vectors
%
%   The file will be saved as a 0-current, and vectors as a vector field.

fid=fopen(filename,'wb');
if(fid<0)
    fprintf('could not open file %s for writing\n',filename);
    return
end
fclose(fid);


% write header
dlmwrite(filename, [ '# vtk DataFile Version 3.0' ],'delimiter','');
dlmwrite(filename, [ 'vtk output' ],'delimiter','','-append');
dlmwrite(filename, [ 'ASCII' ],'delimiter','','-append');
dlmwrite(filename, [ 'DATASET POLYDATA' ],'delimiter','','-append');

% write points
N = size(input.x,2);
if N==2
    x = [input.x zeros(size(input.x,1),1)];
else
    x = input.x;
end

dlmwrite(filename, [ 'POINTS ' num2str(size(input.x,1)) ' float' ],'delimiter','','-append');
dlmwrite(filename, x,'delimiter',' ','-append','precision','%.9f');

if isfield(input,'d')
    nattributes=1; % for now, just d
    % write vectors
    if N==2
        d = [input.d zeros(size(input.d,1),1)];
    else
        d = input.d;
    end
    dlmwrite(filename, [ ' ' ],'delimiter','','-append');
    dlmwrite(filename, [ 'POINT_DATA ' num2str(size(x,1)) ],'delimiter','','-append');
    dlmwrite(filename, [ 'VECTORS d float '],'delimiter','','-append');
    dlmwrite(filename, d ,'delimiter',' ','-append','precision','%.9f');
end
end

