function out=write_MITKPoints(filename, points)

ndimensions  = size(points,1);

bounds = zeros(2*ndimensions,1);
bounds(1:2:end) = min(points,[],2);
bounds(2:2:end) = max(points,[],2);


fid = fopen(filename,'w');
if fid<0
    out = -1;
    return;
end
out = fid;

fprintf(fid,'<?xml version="1.0" encoding="UTF-8" ?>\n');
fprintf(fid,'<point_set_file>\n');
fprintf(fid,'\t<file_version>0.1</file_version>\n');
fprintf(fid,'\t<point_set>\n');
fprintf(fid,'\t\t<time_series>\n');
fprintf(fid,'\t\t\t<time_series_id>0</time_series_id>\n');
fprintf(fid,'\t\t\t<Geometry3D ImageGeometry="false" FrameOfReferenceID="0">\n');
fprintf(fid,'\t\t\t\t<IndexToWorld type="Matrix3x3" m_0_0="1" m_0_1="0" m_0_2="0" m_1_0="0" m_1_1="1" m_1_2="0" m_2_0="0" m_2_1="0" m_2_2="1" />\n');
fprintf(fid,'\t\t\t\t<Offset type="Vector3D" x="0" y="0" z="0" />\n');
fprintf(fid,'\t\t\t\t<Bounds>\n');
fprintf(fid,'\t\t\t\t\t<Min type="Vector3D" x="%f" y="%f" z="%f" />\n',bounds(1), bounds(3), bounds(5));
fprintf(fid,'\t\t\t\t\t<Max type="Vector3D" x="%f" y="%f" z="%f" />\n',bounds(2), bounds(4), bounds(6));
fprintf(fid,'\t\t\t\t</Bounds>\n');
fprintf(fid,'\t\t\t</Geometry3D>\n');
%         /// Iterate over the points
for i =1:size(points,2)
    
    fprintf(fid,'\t\t\t<point>\n');
    fprintf(fid,'\t\t\t\t<id>%d</id>\n',i-1);
    fprintf(fid,'\t\t\t\t<specification>0</specification>\n');
    fprintf(fid,'\t\t\t\t<x>%f</x>\n',points(1,i));
    fprintf(fid,'\t\t\t\t<y>%f</y>\n',points(2,i));
    fprintf(fid,'\t\t\t\t<z>%f</z>\n',points(3,i));
    fprintf(fid,'\t\t\t</point>\n');
    
end
fprintf(fid,'\t\t</time_series>\n');
fprintf(fid,'\t</point_set>\n');
fprintf(fid,'</point_set_file>\n');

fclose(fid);


end
