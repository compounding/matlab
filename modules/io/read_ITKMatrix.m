function m =read_ITKMatrix(filename, varargin)

if nargin==2
    hl = varargin{1};
else
    hl = 1;
end

 fid = fopen(filename, 'r');
 C = textscan(fid, '%s', 4*4, 'HeaderLines', hl)';
 c = str2double(C{1});
 m = reshape(c,4,4)';   
 fclose(fid);

end

