function write_ITKMatrix(filename,M,varargin)
D = size(M,1)-1;
noheader = false;
if numel(varargin)==1
   if strcmp(varargin{1},'noheader')
       noheader = true;
   end
end

 fid = fopen(filename, 'w');
 if ~noheader
    fprintf(fid,['# itkMatrix ' num2str(D+1) ' x ' num2str(D+1) '\n']);
 end
 fprintf(fid,[repmat('%f\t',1,D) '%f\n'],M');
 fclose(fid);

end

