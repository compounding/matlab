function val = dBSplineConv(degree,x)
% val = DBSPLINECONV(degree, x)
%
% computes the value of the convolution of derivtives of the B-Spline of degree 'degree' on the point 'x',
% x can be a vector
% which must be normalised with respect to the bspline grid


switch degree
    case 1
        val = dBc1(x);
    case 3
        val = dBc3(x);
    otherwise
        disp('Bad BSpline degree: only implemented for degrees  1 and 3');
        val=0*x;
end
end

%%
function val = dBc1( x)
    val=0*x;
    
    i1 = find(x>=-2 & x<-1);
    i2 = find(x>=-1 & x<0);
    i3 = find(x>=0 & x<1);
    i4 = find(x>=1 & x<=2);
    
    val(i1) = -x(i1)-2;
    val(i2) = 3*x(i2)+2;
    val(i3) = 2-3*x(i3);
    val(i4) = x(i4)-2;
    
    % The following is the standard convolution but not what we want. we
    % want correlation
    %val(i1) = x(i1)+2;
    %val(i2) = -3*x(i2)-2;
    %val(i3) = 3*x(i3)-2;
    %val(i4) = 2-x(i4);

end
%%

function val = dBc3( x)
    val=0*x;
    
    i1 = find(x>=-3 & x<-2);
    i2 = find(x>=-2 & x<-1);
    i3 = find(x>=-1 & x<0);
    i4 = find(x>=0 & x<1);
    i5 = find(x>=1 & x<2);
    i6 = find(x>=2 & x<=3);
    val(i1) = (7.*x(i1).^5+100.*x(i1).^4+560.*x(i1).^3+1520.*x(i1).^2+1960.*x(i1)+920)/120;
    val(i2) = -(21.*x(i2).^5+180.*x(i2).^4+560.*x(i2).^3+720.*x(i2).^2+280.*x(i2)-24)/120;
    val(i3) = (7.*x(i3).^5+20.*x(i3).^4-32.*x(i3).^2+16)/24;
    val(i4) = -(7.*x(i4).^5-20.*x(i4).^4+32.*x(i4).^2-16)/24;
    val(i5) = (21.*x(i5).^5-180.*x(i5).^4+560.*x(i5).^3-720.*x(i5).^2+280.*x(i5)+24)/120;
    val(i6) = -(7.*x(i6).^5-100.*x(i6).^4+560.*x(i6).^3-1520.*x(i6).^2+1960.*x(i6)-920)/120;
     % The following is the standard convolution but not what we want. we
    % want correlation. n this particular case, just a -1 is enough
%     val(i1) = -(7.*x(i1).^5+100.*x(i1).^4+560.*x(i1).^3+1520.*x(i1).^2+1960.*x(i1)+920)/120;
%     val(i2) = (21.*x(i2).^5+180.*x(i2).^4+560.*x(i2).^3+720.*x(i2).^2+280.*x(i2)-24)/120;
%     val(i3) = -(7.*x(i3).^5+20.*x(i3).^4-32.*x(i3).^2+16)/24;
%     val(i4) = (7.*x(i4).^5-20.*x(i4).^4+32.*x(i4).^2-16)/24;
%     val(i5) = -(21.*x(i5).^5-180.*x(i5).^4+560.*x(i5).^3-720.*x(i5).^2+280.*x(i5)+24)/120;
%     val(i6) = (7.*x(i6).^5-100.*x(i6).^4+560.*x(i6).^3-1520.*x(i6).^2+1960.*x(i6)-920)/120;
        

%val = -val;
%plot(2:0.1:3,dBSpline(3,-1,0:0.1:1)); hold on; plot(1:0.1:2,dBSpline(3,0,0:0.1:1));  plot(0:0.1:1,dBSpline(3,1,0:0.1:1));  plot(-1:0.1:0,dBSpline(3,2,0:0.1:1));hold off;
end




% function val = dBSplineConv(degree, segment,x)
% % val = DBSPLINECONV(degree, segment,x)
% %
% % computes the value of the convolution of derivtives of the B-Spline of degree 'degree' on the point 'x',
% % x can be a vector
% % which must be normalised with respect to the bspline grid
% 
% 
% switch degree
%     case 1
%         val = dBc1(segment,x);
%     case 2
%         val = dBc2(segment,x);
%     case 3
%         val = dBc3(segment,x);
%     case 5
%         val = dBc5(segment,x);
%     otherwise
%         disp('Bad BSpline degree');
%         val=0*x;
% end
% end
% 
% %%
% function val = dBc1(k, x)
% switch k
%     case -1
%         val = 1-x;
%     case 0
%         val=3*x-2; % lower right  B k+1
%     case 1
%         val=1-3*x; % center B k
%     case 2
%         val = x;
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINES OF DEGREE 1');
%         val=0*x;
% end
% end
% %%
% function val = dBc2(k, x)
% switch k
%     case -1
%         val =0;
%     case 0
%         val=0;
%     case 1
%         val=0;
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINE DERIVATIVE OF DEGREE 2');
%         val=0*x;
% end
% end
% %%
% function val = dBc3(k, x)
% switch k
%     case -3
%         val = -(x.^5-5.*x.^4+10.*x.^3-10.*x.^2+5.*x-1)/120;
%     case -2
%             val = (7.*x.^5-30.*x.^4+40.*x.^3-40.*x+24)/120;
%     case -1
%         val = -(21.*x.^5-75.*x.^4+50.*x.^3+90.*x.^2-95.*x-15)/120;
%     case 0
%         val = (7.*x.^5-20.*x.^4+32.*x.^2-16)/24;
%     case 1
%         val = -(7.*x.^5-15.*x.^4-10.*x.^3+18.*x.^2+19.*x-3)/24;
%     case 2
%         val=(21.*x.^5-30.*x.^4-40.*x.^3+40.*x+24)/120;
%     case 3
%         val=-(7.*x.^5-5.*x.^4-10.*x.^3-10.*x.^2-5.*x-1)/120;
%     case 4
%         val = x.^5/120; %rightmost
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINES DERIVATIVE OF DEGREE 3');
%         val=0*x;
% end
% %val = -val;
% %plot(2:0.1:3,dBSpline(3,-1,0:0.1:1)); hold on; plot(1:0.1:2,dBSpline(3,0,0:0.1:1));  plot(0:0.1:1,dBSpline(3,1,0:0.1:1));  plot(-1:0.1:0,dBSpline(3,2,0:0.1:1));hold off;
% end
% 
% function val = dBc5(k, x)
% switch k
%     case -2
%         val = 0; %rightmost
%     case -1
%         val =0;
%     case 0
%         val=0;
%     case 1
%         val=0;
%     case 2
%         val = 0;
%     case 3
%         val = 0; % leftmost
%     otherwise
%         val=0*x;
% end
% %val = -val;
% %plot(2:0.1:3,dBSpline(3,-1,0:0.1:1)); hold on; plot(1:0.1:2,dBSpline(3,0,0:0.1:1));  plot(0:0.1:1,dBSpline(3,1,0:0.1:1));  plot(-1:0.1:0,dBSpline(3,2,0:0.1:1));hold off;
% end
% 
