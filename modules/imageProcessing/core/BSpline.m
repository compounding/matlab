function val = BSpline(degree, x)
% val = BSPLINE(degree, x)
%
% computes the value of the B-Spline of degree 'degree' on the point 'x',
% x can be a vector
% which must be normalised with respect to the bspline grid


switch degree
    case 1
        val = B1(x);
    case 3
        val = B3(x);
    otherwise
        disp('Bad BSpline degree');
        val=0*x;
end
end

%%
function val = B1(x)

    val=0*x;
    
    i1 = find(x>=-1 & x<0);
    i2 = find(x>=0 & x<=1);

    val(i1) = x(i1)+1;
    val(i2) = 1-x(i2);

end
% %%
% function val = B2(k, x)
% switch k
%     case -1
%         val = (x - 1.0) .* (x - 1.0) ./ 2.0;
%     case 0
%         val=(-2.0.* x .* x + 2.0.* x +1.0) ./ 2.0;
%     case 1
%         val=x .* x ./ 2.0;
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINES OF DEGREE 2');
%         val=0*x;
% end
% end
%%
function val = B3( x)

    val=0*x;
    
    i1 = find(x>=-2 & x<-1);
    i2 = find(x>=-1 & x<0);
    i3 = find(x>=0 & x<1);
    i4 = find(x>=1 & x<2);

    
    val(i1) = (x(i1).^3+6.*x(i1).^2+12.*x(i1)+8)/6;
    val(i2) = -(3.*x(i2).^3+6.*x(i2).^2-4)/6;
    val(i3) = (3.*x(i3).^3-6.*x(i3).^2+4)/6;
    val(i4) = -(x(i4).^3-6.*x(i4).^2+12.*x(i4)-8)/6;
end
%%
% function val = B5(k, x)
% switch k
%     case -2
%         val = x.^5/120; % rightmost
%     case -1
%         val = -(5*x.^5-5*x.^4-10*x.^3-10*x.^2-5*x-1)/120;
%     case 0
%         val=(5*x.^5-10*x.^4-10*x.^3+10*x.^2+25*x+13)/60;
%     case 1
%         val=-(5*x.^5-15*x.^4+30*x.^2-33)/60;
%     case 2
%         val = (5*x.^5-20*x.^4+20*x.^3+20*x.^2-50*x+26)/120;
%     case 3
%         val = -(x-1).^5/120; % leftmost
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINES OF DEGREE 3');
%         val=0*x;
% end
%end

