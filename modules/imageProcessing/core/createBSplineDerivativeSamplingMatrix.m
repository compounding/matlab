function [dS,nodes_to_remove_, used_positions] = createBSplineDerivativeSamplingMatrix(positions,gr,varargin)
% dS = createBSplineDerivativeSamplingMatrix
%   positions must be  a N x K array, where N is the number of points and K
%   the input dimensionality
%
% This function works with any input dimensionality and any output
% dimensionality. The latter is to be passed as an argument.
%
% the output is a matrix dS = [dSx  ; dSy ; dSz ...]
% Reordering is not used unles divergence is selected


bsd=3;
output_dimensionality = 1; % by default, scalar function
reorder = false; % only makes sense if output dimensionality >=2
remove_empty_nodes=false;
nodes_to_remove_=[];
normalize = false;
normalize_cell=[];
order=1; % order of the derivative
operator='div'; % options are: 'div', 'grad'
% read args
i=1;
while (i <= size(varargin,2))
    if (strcmp( varargin{i} , 'bsd'))
        bsd = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'od')) % not required!
        output_dimensionality = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'remove_empty_nodes'))
        remove_empty_nodes = true;
        remove_empty_nodes_th= varargin{i+1};
        i = i+1;
    elseif(strcmp( varargin{i} , 'reorder'))
         reorder= varargin{i+1};
    elseif(strcmp( varargin{i} , 'operator'))
        operator= varargin{i+1};
    elseif(strcmp( varargin{i} , 'order'))
        order= varargin{i+1};
    elseif(strcmp( varargin{i} , 'normalize'))
        % normalize will multiply each point times the area of influence of
        % the point (provided by normalize_cell). Not that the final area
        % will be squared because of the div squared.
        normalize= true;
        normalize_cell= varargin{i+1};
        i=i+1;
    end
    i = i+1;
end

input_dimensionality = size(positions,2);

% create the neighbourhood kernel

m=0-ceil(bsd/2);
M=floor(bsd/2);

% the following is equivalent to this:
% [x, y, z]=ndgr(m:M,m:M,m:M);grpos = [x(:) y(:) z(:)]; clear x y z;
str1='';
str2='';
str3='';
str4='';
for i=1:input_dimensionality
    str1=[str1 'm:M,'] ;
    str2=[str2 'x' num2str(i) '(:) '] ;
    str3=[str3 'x' num2str(i) ', ']    ;
    str4=[str4 'x' num2str(i) ' ']    ;
end
if input_dimensionality==1
    grpos = (m:M)';
else
    eval(['[' str3(1:end-1) ']=ndgrid(' str1(1:end-1) ');grpos = [' str2(1:end-1) ']; clear ' str4 ';' ]);
end
clear str1 str2 str3 str4;

%get indexes of this positions with respect to the B-Spline gr
gr_positions= gr.GetContinuousIndex(positions')'; 
% convert grnodes to single index

%Build matrix Ss (spline sampling)
%first segment (1 band of the matrix)
ncols = numel(gr.data); % number of grpoints
nrows = size(positions,1); % number of input points, equal to npts


rows = uint32([]); % this might save a bit of memory
cols = uint32([]);
for i=1:input_dimensionality
    eval(['sds_' num2str(i) '=[];']);
end
used_positions = [];
for node=1:size(grpos,1)
    
    % remove any points that for this hernel (in  grpos) is outside of
    % the gr. This can happen for example if reguilarization is imposed
    % everywhere
    
    % columns: 1 per current grpoint
    gr_nodes = gr_positions - floor(gr_positions)  + ones(size(gr_positions,1),1)*grpos(node,:); 
    associated_gr_nodes = floor(gr_positions - ones(size(gr_positions,1),1)*grpos(node,:));
    toremove =[];
    for i=1:input_dimensionality
        s__(i,:) = BSpline(bsd,gr_nodes(:,i)'); 
        toremove = [toremove  find(s__(i,:)<=0 )];
        topreserve = setdiff(1:size(gr_nodes,1),toremove);
        if order==1
            ds_(i,:) = dBSpline(bsd,gr_nodes(:,i)')/gr.spacing(i);
        elseif order==2
            ds_(i,:) = dBSpline2(bsd,gr_nodes(:,i)')/gr.spacing(i)^2;
        end
    end
    
    
    
    minimums= min(associated_gr_nodes);
    maximums = max(associated_gr_nodes);
    if numel(find(minimums'<gr.index)) || numel(find(gr.size(1:input_dimensionality)+gr.index-1-maximums'<0))
        %disp('Warning: some points were out of the gr. This is not necesarily bad but this should not happen if the gr totally encloses input data')
        [rmin,~]=find(associated_gr_nodes - ones(size(associated_gr_nodes,1),1)*gr.borderIndex1' < 0);
        if numel(gr.spacing)==1
            %[rmax,~] = find(ones(size(associated_gr_nodes,1),1)*(gr.size(1)+gr.index(1)-1)-associated_gr_nodes<0);
            [rmax,~] = find(ones(size(associated_gr_nodes,1),1)*(gr.borderIndex2(1))-associated_gr_nodes<0);
        else
            %[rmax,~] = find(ones(size(associated_gr_nodes,1),1)*(gr.size'+gr.index'-1)-associated_gr_nodes<0);
            [rmax,~] = find(ones(size(associated_gr_nodes,1),1)*(gr.borderIndex2')-associated_gr_nodes<0);
        end
        toremove = [toremove rmin(:)' rmax(:)'];
    end
    
    
    toremove = unique(toremove);
    topreserve = setdiff(1:size(gr_nodes,1),toremove);
    s__ = s__(:,topreserve);
    ds_ = ds_(:,topreserve);
   
    
    %toremove = unique(toremove);
    %topreserve = setdiff(1:size(gr_nodes,1),toremove);
    %used_positions = [used_positions topreserve];
    
    npts = numel(s__(1,:));
    % the following is equivalent to this:
    % s1 = [s1 dsx.*sy.*sz]; % row array
    % s2 = [s2 sx.*dsy.*sz]; % row array
    % s3 = [s3 sx.*sy.*dsz]; % row array
    str1='';
    str2='';
    for i=1:input_dimensionality
        str1=[str1 ' ''ds_(' num2str(i) ',:).*'' ;'] ;
        str2=[str2 ' ''s__(' num2str(i) ',:).*'' ;'] ;
    end
    eval(['ds_array = [' str1(1:end-1) ']; s_array = [' str2(1:end-1) ']; ' ]);
    clear str1 str2;
    str_s = repmat('s__(x,:).*',input_dimensionality,1);
    
    for i=1:input_dimensionality
        number = setdiff(1:input_dimensionality,i);
        str_s(i,:)=ds_array(i,:);
        str_s(number,:)=s_array(number,:);
        
        string_dbsp = reshape(str_s',1,[]);
        eval(['sds_' num2str(i) '=[sds_' num2str(i) ' ' string_dbsp(1:end-2)  '];']);
    end
    clear s__ ds_ str_s;
    
   
    gr_indices = gr.nd_to_oned_index(associated_gr_nodes(topreserve,:)');
    
    clear str1 gr_nodes;
    
    cols = [cols  gr_indices']; % row vector
    clear gr_indices;
    % rows: 1 per point
    
    rows = [rows topreserve]; % row vector
   
end

if normalize % this will do the sum as: E_k c_k*b(x/a-k)*Ak, where Ak = normalize_cell(1)
    for i=1:input_dimensionality
        eval(['sds_' num2str(i) '= sds_' num2str(i) '*normalize_cell(' num2str(i) ');']);
    end
end

clear floor_continuous_indexes gr_positions;

if strcmp(operator,'div') && order==1
    %% divergence
    
    if output_dimensionality<2
        disp('ERROR: divergence only makes sense if the output is a verctor quantity')
        dS=[];
        return;
    end
    
    if reorder
        % make it more diagonal by reordering rows to put x y z of each point
        % together
        
        % the following is equivalent to this:
        % totalrows = [rows rows rows];
        % totalcols = [(cols-1)*3+1  (cols-1)*3+2 (cols-1)*3+3];
        str1='';
        str2='';
        for i=1:input_dimensionality % because this is required for gradient/divergence
            %for i=1:output_dimensionality
            str1=[str1 'rows '] ;
            %str2=[str2 '(cols-1)*' num2str(output_dimensionality) '+' num2str(i) ' '] ;
            str2=[str2 '(cols-1)*' num2str(input_dimensionality) '+' num2str(i) ' '] ;
        end
        eval(['totalrows = [' str1(1:end-1) ']; ']);
        eval(['totalcols = [' str2(1:end-1) '];']);
        clear str1 str2;
    else
        
        % the following is equivalent to this:
        % totalrows = [rows  rows rows];
        % totalcols = [cols  cols+ncols  cols+2*ncols];
        str1='';
        str2='';
        for i=1:input_dimensionality % because this is required for gradient/divergence
            %for i=1:output_dimensionality
            str1=[str1 'rows '] ;
            str2=[str2 'cols+' num2str(i-1) '*ncols '] ;
        end
        eval(['totalrows = [' str1(1:end-1) ']; ']);
        eval(['totalcols = [' str2(1:end-1) '];']);
        clear str1 str2;
    end
    
else
    
    % create  the matrix dSx, dSy, etc separately.
    
    % the following is equivalent to this:
    % totalrows = [rows  rows+nrows rows+2*nrows];
    % totalcols = [cols];
    str1='';
    str2='';
    for i=1:input_dimensionality % because this is required for gradient/divergence
        str1=[str1 'rows+' num2str(i-1) '*nrows '] ;
        str2=[str2 'cols '] ;
    end
    eval(['totalrows = [' str1(1:end-1) ']; ']);
    eval(['totalcols = [' str2(1:end-1) '];']);
    clear str1 str2;
    

end
clear rows cols;

% the following is equivalent to this:

% This has to be corrected depending on: is this gradient or is this
% divergence?. Maybe the best is to returs Sx, Sy and Sz and then the user
% does whatever he needs...

% dS = sparse(double(totalrows),double(totalcols) ,[ s1 s2 s3] ,nrows,3*ncols) ;
str1='';
for i=1:input_dimensionality % because this is required for gradient/divergence
    %for i=1:output_dimensionality
    str1=[str1 'sds_' num2str(i) ' ' ] ;
end
%eval(['dS = sparse(double(totalrows),double(totalcols) ,[' str1(1:end-1) '] , nrows, ' num2str(output_dimensionality) '*ncols) ;']);

nfinalrows = max(totalrows) + double((mod(max(totalrows),nrows)~=0))*(nrows-mod(max(totalrows),nrows));
nfinalcols = max(totalcols) + double((mod(max(totalcols),ncols)~=0))*(ncols-mod(max(totalcols),ncols));

eval(['dS = sparse(double(totalrows),double(totalcols) ,[' str1(1:end-1) '] , ' num2str(nfinalrows) ', ' num2str(nfinalcols) ') ;']);
clear str1 s totalrows totalcols;

%% Remove empty nodes

if remove_empty_nodes
    if numel(remove_empty_nodes_th)>1
        nodes_to_remove = find(remove_empty_nodes_th);
        if output_dimensionality==2 && reorder
            nodes_to_remove = find(remove_empty_nodes_th(1:2:end));
        end
    else
        normalised_aggregated_node_value = (sum(dS)/max(sum(dS))).^(1/input_dimensionality);
        nodes_to_remove = find(normalised_aggregated_node_value<remove_empty_nodes_th);
    end
    %nodes_to_remove_ = zeros(1,size(dS,2));
    nodex_to_remove_1D = nodes_to_remove(input_dimensionality:input_dimensionality:end)/input_dimensionality;
    dS(:,nodex_to_remove_1D)=[];
    nodes_to_remove_(nodes_to_remove)=1;
end

used_positions = unique(used_positions);


end