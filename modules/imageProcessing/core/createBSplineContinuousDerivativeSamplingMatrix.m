function [P,nodes_to_remove_] = createBSplineContinuousDerivativeSamplingMatrix(gr,varargin)
% dS = createBSplineContinuousDerivativeSamplingMatrix
%
%
% This function works with any input dimensionality and any output
% dimensionality. The latter is to be passed as an argument.
%
% This function calculates the exact integral of the derivative quantity, using the convolution values.
% the output is a matrix P
% This function is coded for the gradient function


bsd=3;
output_dimensionality = 1; % by default, scalar function
reorder = true; % only makes sense if output dimensionality >=2
remove_empty_nodes=false;
nodes_to_remove_=[];
operator='grad'; % options are: 'div', 'grad'
% read args
i=1;
while (i <= size(varargin,2))
    if (strcmp( varargin{i} , 'bsd'))
        bsd = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'od'))
        output_dimensionality = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'operator'))
        operator= varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'remove_empty_nodes'))
        remove_empty_nodes = true;
        remove_empty_nodes_th= varargin{i+1};
        i = i+1;
    elseif(strcmp( varargin{i} , 'reorder'))
        reorder= varargin{i+1};
    end
    i = i+1;
end

if output_dimensionality==1 && ~strcmp(operator,'grad')
    disp('Switching to ''grad'' operator')
    operator='grad';
end

input_dimensionality = numel(gr.spacing);

% create indices k,m and l,n
% the following would be equivalent to something like
% [x1,x2] =  ndgr(1:gr.size(1),1:gr.size(1)); indices = [x1(:) x2(:)]
str1='';
str2='';
str3='';
str4='';
for i=1:input_dimensionality
    str1=[str1 '1:gr.size(' num2str(i) '),'] ;
    str2=[str2 'x' num2str(i) '(:) '] ;
    str3=[str3 'x' num2str(i) ', ']    ;
    str4=[str4 'x' num2str(i) ' ']    ;
end
eval(['[' str3(1:end-1) ']=ndgrid(' str1(1:end-1) ');indices = [' str2(1:end-1) ']; clear ' str4 ';' ]);



% create the matrix P_ (for each dimension). This matrix has the incides
% (k-m), (l-n), etc that will be used to calculate matrices P
P_ = zeros(size(indices,1),size(indices,1),input_dimensionality);
for k=1:input_dimensionality
    P_(:,:,k) = indices(:,k)*ones(1,size(indices,1))-ones(size(indices,1),1)*indices(:,k)';
end

for k=1:input_dimensionality
    rows=[];
    cols=[];
    vals_dBSplineConv = [];
    vals_BSplineConv = [];
    for i=1:size(indices,1)
       idx=find(abs(indices(:,k)-indices(i,k))<=2*bsd);
       cols = [cols; idx];
       rows = [rows; ones(numel(idx),1)*i];
       %vals = [vals ; -indices(idx,k)+indices(i,k)];
       vals_dBSplineConv = [vals_dBSplineConv ; dBSplineConv(bsd,-indices(idx,k)+indices(i,k)) ];
       vals_BSplineConv = [vals_BSplineConv ; BSplineConv(bsd,-indices(idx,k)+indices(i,k)) ];
    end
    P_BSplineConv{k} = sparse(rows,cols ,vals_BSplineConv);
    P_dBSplineConv{k} = sparse(rows,cols ,vals_dBSplineConv);
end
clear rows cols vals_dBSplineConv vals_BSplineConv;
% derivative along each dimension
% The following does something like this
% Pxx = prod(gr.spacing)*dBSplineConv(3,P_(:,:,1))*prod(BSplineConv(3,P_(:,:,[2 3])),3);
% Pyy = prod(gr.spacing)*dBSplineConv(3,P_(:,:,2))*prod(BSplineConv(3,P_(:,:,[1 3])),3);
% Pzz = prod(gr.spacing)*dBSplineConv(3,P_(:,:,3))*prod(BSplineConv(3,P_(:,:,[1 2])),3);

%Pxx=zeros(size(indices,1),size(indices,1),input_dimensionality);
for k=1:input_dimensionality
    other_dimensions = setdiff(1:input_dimensionality,k);
    %Pxx(:,:,k)=prod(gr.spacing)*dBSplineConv(bsd,P_(:,:,k)).*prod(BSplineConv(bsd,P_(:,:,other_dimensions)),3);
    %other = BSplineConv(bsd,P_{other_dimensions(1)});
    if numel(other_dimensions)
        other = P_BSplineConv{other_dimensions(1)};
        for i=2:numel(other_dimensions)
            other = other.*P_BSplineConv{other_dimensions(i)};
        end
    else
        other=1;
    end
    Pxx{k}=prod(gr.spacing)*P_dBSplineConv{k}.*other;
end
clear P_BSplineConv P_dBSplineConv;

if strcmp(operator,'grad')

    P = Pxx{1};
    for i=2:numel(Pxx)
        P=P+Pxx{i};
    end
    
%     P = [];
%     for i=1:numel(Pxx)
%         P = [ P ;  repmat(Pxx{i}*0,1,i-1) Pxx{i} repmat(Pxx{i}*0,1,numel(Pxx)-i)];
%     end
%     
%     if reorder && output_dimensionality>1
%         % the following lines do the following:
%         %P(:,[1:output_dimensionality:end 2:output_dimensionality:end ]) = P;
%         %P([1:output_dimensionality:end 2:output_dimensionality:end ],:)=P;
%         str1='';
%         for k=1:input_dimensionality
%             str1 = [str1 num2str(k) ':output_dimensionality:end '] ;
%         end
%         eval([ 'P(:,[' str1(1:end-1) ']) = P;']);
%         eval([ 'P([' str1(1:end-1) '],:) = P;']);
%     end
    
    
elseif strcmp(operator,'div')
    
    % in addition to the Pxx matrices, we need the crossed matrices: Pxy,
    % Pxz, etc
    Pxy = zeros(size(indices,1),size(indices,1),input_dimensionality,input_dimensionality);
    
    str1='';
    for k=1:input_dimensionality
        for j=1:input_dimensionality
            if k==j
                str1=[str1 ' Pxx{' num2str(k) '}(:,:)'];
                continue;
            end
            str1=[str1 ' Pxy(:,:,' num2str(k) ',' num2str(j) ')'];
            
            Pxy(:,:,k,j)=prod(gr.spacing)*BSplineCrossedConv(bsd,P_(:,:,k),-1).*BSplineCrossedConv(bsd,P_(:,:,j),1); % this is actially
            other_dimension = setdiff(1:input_dimensionality,[j k]);
            if numel(other_dimension)
                Pxy(:,:,k,j) = Pxy(:,:,k,j).*prod(BSplineConv(bsd,P_(:,:,other_dimension)),3);
            end
        end
        str1=[str1 ';'];
    end
    % The next line does the following:
    % P = [Pxx(:,:,1) Pxy(:,:,1,2); Pxy(:,:,2,1) Pxx(:,:,2) ];
    eval([ 'P = [' str1(1:end-1) '];']);
    
    if reorder
        % the following lines do the following:
        %P(:,[1:output_dimensionality:end 2:output_dimensionality:end ]) = P;
        %P([1:output_dimensionality:end 2:output_dimensionality:end ],:)=P;
        str1='';
        for k=1:input_dimensionality
            str1 = [str1 num2str(k) ':output_dimensionality:end '] ;
        end
        eval([ 'P(:,[' str1(1:end-1) ']) = P;']);
        eval([ 'P([' str1(1:end-1) '],:) = P;']);
        
    end
    
else
    disp('Wrong operator: must be ''div'' or ''grad''');
    P=[];
    return;
end

if remove_empty_nodes
    if numel(remove_empty_nodes_th)>1
        nodes_to_remove = find(remove_empty_nodes_th);
        if output_dimensionality==2 && reorder && strcmp(operator,'grad')
            nodes_to_remove = find(remove_empty_nodes_th(1:2:end));
        elseif output_dimensionality==2 && reorder && strcmp(operator,'div')
            nodes_to_remove = find(remove_empty_nodes_th);
        end
    else
        disp('Something is wrong! Specify an array of nodes to remove');
    end
    
    P(:,nodes_to_remove)=[];
    P(nodes_to_remove,:)=[];
    
    
    nodes_to_remove_=remove_empty_nodes_th;
end


end