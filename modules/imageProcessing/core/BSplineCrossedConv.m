function val = BSplineCrossedConv(degree,x,order)
% val = DBSPLINECROSSEDCONV(degree, x,order)
%
% computes the value of the convolution of the B-spline and of derivtives of the B-Spline of degree 'degree' on the point 'x',
% x can be a vector
% which must be normalised with respect to the bspline grid
%
% order can be -1 dN(-x)*N(x) or 1 dN(x)*N(-x). For the particular case of
% B-splines, only a sign changes


switch degree
    case 1
        val = Bcc1(x,order);
    case 3
        val = Bcc3(x,order);
    otherwise
        disp('Bad BSpline degree: only implemented for degrees  1 and 3');
        val=0*x;
end
end

%%
function val = Bcc1( x,order)
    val=0*x;
    
    i1 = find(x>=-2 & x<-1);
    i2 = find(x>=-1 & x<0);
    i3 = find(x>=0 & x<1);
    i4 = find(x>=1 & x<=2);
    
    val(i1) = -(x(i1).^2+4.*x(i1)+4)/2;
    val(i2) = (3.*x(i2).^2+4.*x(i2))/2;
    val(i3) = -(3.*x(i3).^2-4.*x(i3))/2;
    val(i4) = (x(i4).^2-4.*x(i4)+4)/2;    
    
    val = val*order;

end
%%

function val = Bcc3( x,order)
    val=0*x;
    
    i1 = find(x>=-3 & x<-2);
    i2 = find(x>=-2 & x<-1);
    i3 = find(x>=-1 & x<0);
    i4 = find(x>=0 & x<1);
    i5 = find(x>=1 & x<2);
    i6 = find(x>=2 & x<=3);
    
    val(i1) = -(7.*x(i1).^6+120.*x(i1).^5+840.*x(i1).^4+3040.*x(i1).^3+5880.*x(i1).^2+5520.*x(i1)+1736)/720;
    val(i2) = (21.*x(i2).^6+216.*x(i2).^5+840.*x(i2).^4+1440.*x(i2).^3+840.*x(i2).^2-144.*x(i2)+56)/720;
    val(i3) = -(7.*x(i3).^6+24.*x(i3).^5-64.*x(i3).^3+96.*x(i3))/144;
    val(i4) = (7.*x(i4).^6-24.*x(i4).^5+64.*x(i4).^3-96.*x(i4))/144;
    val(i5) = -(21.*x(i5).^6-216.*x(i5).^5+840.*x(i5).^4-1440.*x(i5).^3+840.*x(i5).^2+144.*x(i5)+56)/720;
    val(i6) = (7.*x(i6).^6-120.*x(i6).^5+840.*x(i6).^4-3040.*x(i6).^3+5880.*x(i6).^2-5520.*x(i6)+1736)/720;
    
    val = val*order;
        
end

