function val = BSplineConv(degree,x)
% val = BSPLINECONV(degree, x)
%
% computes the value of the convolution of  the B-Spline of degree 'degree' on the point 'x',
% x can be a vector
% which must be normalised with respect to the bspline grid


switch degree
    case 1
        val = Bc1(x);
    case 3
        val = Bc3(x);
    otherwise
        disp('Bad BSpline degree: only implemented for degrees  1 and 3');
        val=0*x;
end
end

%%
function val = Bc1( x)
    val=0*x;
    
    i1 = find(x>=-2 & x<-1);
    i2 = find(x>=-1 & x<0);
    i3 = find(x>=0 & x<1);
    i4 = find(x>=1 & x<=2);
    
    val(i1) = (x(i1).^3+6.*x(i1).^2+12.*x(i1)+8)/6;
    val(i2) = -(3.*x(i2).^3+6.*x(i2).^2-4)/6;
    val(i3) = (3.*x(i3).^3-6.*x(i3).^2+4)/6;
    val(i4) = -(x(i4).^3-6.*x(i4).^2+12.*x(i4)-8)/6;    
    
    % The following is the standard convolution but not what we want. we
    % want correlation (in this case, it is the same equation though)
    %val(i1) = (x(i1).^3+6.*x(i1).^2++12.*x(i1)+8)/6;
    %val(i2) = -(3.*x(i2).^3+6.*x(i2).^2-4)/6;
    %val(i3) = (3.*x(i3).^3-6.*x(i3).^2+4)/6;
    %val(i4) = -(x(i4).^3-6.*x(i4).^2+12.*x(i4)-8)/6;

end
%%

function val = Bc3( x)
    val=0*x;
    
    i1 = find(x>=-3 & x<-2);
    i2 = find(x>=-2 & x<-1);
    i3 = find(x>=-1 & x<0);
    i4 = find(x>=0 & x<1);
    i5 = find(x>=1 & x<2);
    i6 = find(x>=2 & x<=3);
    
    % The following is the standard convolution but not what we want. we
    % want correlation (in this case, it is the same equation though)
    val(i1) = -(7.*x(i1).^7+140.*x(i1).^6+1176.*x(i1).^5+5320.*x(i1).^4+13720.*x(i1).^3+19320.*x(i1).^2+12152.*x(i1)+1112)/5040;
    val(i2) = (21.*x(i2).^7+252.*x(i2).^6+1176.*x(i2).^5+2520.*x(i2).^4+1960.*x(i2).^3-504.*x(i2).^2+392.*x(i2)+2472)/5040;
    val(i3) = -(35.*x(i3).^7+140.*x(i3).^6-560.*x(i3).^4+1680.*x(i3).^2-2416)/5040;
    val(i4) = (35.*x(i4).^7-140.*x(i4).^6+560.*x(i4).^4-1680.*x(i4).^2+2416)/5040;
    val(i5) = -(21.*x(i5).^7-252.*x(i5).^6+1176.*x(i5).^5-2520.*x(i5).^4+1960.*x(i5).^3+504.*x(i5).^2+392.*x(i5)-2472)/5040;
    val(i6) = (7.*x(i6).^7-140.*x(i6).^6+1176.*x(i6).^5-5320.*x(i6).^4+13720.*x(i6).^3-19320.*x(i6).^2+12152.*x(i6)-1112)/5040;
        
end

