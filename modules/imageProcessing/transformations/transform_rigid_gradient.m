function grad_h = transform_rigid_gradient(  x, params, varargin )
%TRANSFORM_RIGID_GRADIENT Compute gradient of rigid transformation
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2013

transform_initialization = [];
dbg = false;
reorder = true;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'no_reorder'))
        reorder = false;
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
    
end

NPARAMS = numel(params);

NDIMSO = transform_initialization.output_dimensionality;
noutputdimensions = NDIMSO;
grad_h = zeros(size(x,1)*noutputdimensions,NPARAMS);

if numel(params)==3
    tx = params(1);
    ty = params(2);
    a = params(3);
    
    grad_h(:,1 ) = g_tx(params, x, tx,ty,0,a,0,0);
    grad_h(:,2 ) = g_ty(params, x, tx,ty,0,a,0,0);
    grad_h(:,3 ) = g_a(params,x, tx,ty,0,a,0,0);
    
elseif numel(params)==6
    tx = params(1);
    ty = params(2);
    tz = params(3);
    a = params(4);
    b = params(5);
    c = params(6);
    
    grad_h(:,1 ) = g_tx(params,x, tx,ty,tz,a,b,c);
    grad_h(:,2 ) = g_ty(params,x, tx,ty,tz,a,b,c);
    grad_h(:,3 ) = g_tz(params,x, tx,ty,tz,a,b,c);
    grad_h(:,4 ) = g_a(params,x, tx,ty,tz,a,b,c);
    grad_h(:,5) = g_b(params,x, tx,ty,tz,a,b,c);
    grad_h(:,6 ) = g_c(params,x, tx,ty,tz,a,b,c);
    
end





% grad_h has as many rows as input data points x, and as many columns as
% noutputdimension x nparams

end


function val = g_tx(params, x, tx,ty,tz,a,b,c )
if numel(params)==3
    w = [0 0]';
elseif numel(params)==6
    w = [0 0 0]';
end
w(1)=1;
val = repmat(w,size(x,1),1);
end
%
function val = g_ty(params, x, tx,ty,tz,a,b,c )
if numel(params)==3
    w = [0 0]';
elseif numel(params)==6
    w = [0 0 0]';
end
w(2)=1;
val = repmat(w,size(x,1),1);
end
%
function val = g_tz(params, x, tx,ty,tz,a,b,c )
if numel(params)==3
    w = [0 0]';
elseif numel(params)==6
    w = [0 0 0]';
end
w(3)=1;
val = repmat(w,size(x,1),1);
end
%
function val = g_a(params,x, tx,ty,tz,a,b,c)
if numel(params)==3
    val = [ -cos(a)*x(:,2)-sin(a)*x(:,1) ...
            cos(a)*x(:,1)]';
elseif numel(params)==6
    val = [ cos(a)*cos(b)*sin(c)*x(:,3)+sin(a)*sin(c)*x(:,2)-cos(a)*sin(b)*sin(c)*x(:,1) ...
        -cos(a)*cos(b)*cos(c)*x(:,3)-sin(a)*cos(c)*x(:,2)+cos(a)*sin(b)*cos(c)*x(:,1)  ...
        -sin(a)*cos(b)*x(:,3)+cos(a)*x(:,2)+sin(a)*sin(b)*x(:,1)]';
end
val = val(:);
end
%
function val = g_b(params,x, tx,ty,tz,a,b,c)
val = [(cos(b)*cos(c)-sin(a)*sin(b)*sin(c))*x(:,3)+(-sin(a)*cos(b)*sin(c)-sin(b)*cos(c))*x(:,1) ...
    (cos(b)*sin(c)+sin(a)*sin(b)*cos(c))*x(:,3)+(sin(a)*cos(b)*cos(c)-sin(b)*sin(c))*x(:,1) ...
    -cos(a)*sin(b)*x(:,3)-cos(a)*cos(b)*x(:,1)]';
val = val(:);
end
%
function val = g_c(params,x, tx,ty,tz,a,b,c)
val = [(sin(a)*cos(b)*cos(c)-sin(b)*sin(c))*x(:,3)-cos(a)*cos(c)*x(:,2)+(-cos(b)*sin(c)-sin(a)*sin(b)*cos(c))*x(:,1) ...
    (sin(a)*cos(b)*sin(c)+sin(b)*cos(c))*x(:,3)-cos(a)*sin(c)*x(:,2)+(cos(b)*cos(c)-sin(a)*sin(b)*sin(c))*x(:,1) ...
    0*x(:,1)]';
val = val(:);
end
