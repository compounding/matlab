function x = parametersFromRigidMatrixITKEuler(M,varargin)
% PARAMETERSFROMRIGIDMATRIX Computes the the rigid paramrters from rigid matrix
% Data from the maxima script
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2015

c0 = [];

for i=1:numel(varargin)
   if strcmp(lower(varargin{i}),'centreoftransformation')
       c0 = varargin{i+1};
   end
end

if numel(c0)
    CORMatrix = eye(3+1);
    CORMatrix(1:3, 3+1) = -c0;
    % Move the image to be centred about the centreofrotation and then after
    % the rotation undo.
    M = CORMatrix \  M * CORMatrix;
end

ndims = size(M,1)-1;
x(1:ndims) = M(1:ndims,ndims+1);

if ndims==3
   
    cf = @(angles) (sum(diag(M\rigidMatrixFromParametersITKEuler([x angles])))-4).^2;
    xout = fminsearch(cf,[0 0 0]);
    x = [x xout];

else 
    
    cf = @(angles) (sum(diag(M\rigidMatrixFromParametersITKEuler([x angles])))-4).^2;
    xout = fminsearch(cf,0);
    x = [x xout];
end

end