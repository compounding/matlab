function transform_initialization = transform_FFDBsplines_initialize(  input_image, transform_params, varargin )
%TRANSFORM_FFDBSPLINES Summary of this function goes here
%   Detailed explanation goes here

%transform_params.nlevels=1;
%transform_params.bsd=3;
%transform_params.bounds=-1; % if -1 then take from image
% transform_params.grid_spacing

dbg = false;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
end

if transform_params.bounds(1)<0
    transform_params.bounds = input_image.GetBounds();
end

transform_initialization.bsd = transform_params.bsd;
transform_initialization.output_dimensionality = numel(transform_params.bounds)/2;
transform_initialization.bounds = transform_params.bounds;
transform_initialization.nlevels = transform_params.nlevels;
if size(transform_params.grid_spacing,2)==1 && transform_params.nlevels>1
    transform_params.grid_spacing = [transform_params.bounds(2:2:end)-transform_params.bounds(1:2:end) transform_params.grid_spacing];
end
for i=1:transform_params.nlevels
    
    if transform_params.nlevels==1
        current_spacing =  transform_params.grid_spacing;
    else
        current_spacing = transform_params.grid_spacing(:,1)*(transform_params.nlevels-i)/(transform_params.nlevels-1) ...
            + transform_params.grid_spacing(:,2)*(i-1)/(transform_params.nlevels-1);
    end
    
    currentGrid = createBSplineGrid(transform_params.bounds,transform_params.bsd,current_spacing);
    if transform_initialization.output_dimensionality >1
        currentGrid = VectorPatchType(currentGrid);
    end
    transform_initialization.grid(i) = currentGrid;
    transform_initialization.params0{i} = zeros(transform_initialization.output_dimensionality*numel(transform_initialization.grid(i).data(:)),1); % Starting guess
    
end

end

