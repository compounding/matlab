function x = parametersFromAffineMatrix(M)
% PARAMETERSFROMRIGIDMATRIX Computes the the rigid paramrters from rigid matrix
% Data from the maxima script
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2015

ndims = size(M,1)-1;
x(1:ndims) = M(1:ndims,ndims+1);



if ndims==3
    x_unknown = [0 0 0 1 1 1 0 0 0];
else
    x_unknown = [0 1 1 0 0];
end

cf = @(angles) (sum(diag(M\affineMatrixFromParameters([x angles])))-4).^2;
xout = fminsearch(cf,x_unknown);
x = [x xout];

end