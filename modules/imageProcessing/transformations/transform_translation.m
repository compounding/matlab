function [ output_image ] = transform_translation(  input_image, c, varargin )
%TRANSFORM_TRANSLATION  Transforms the input ND image using the translation parameters in c
%   This function transforms the input image (ImageType) using the input translation parameters specified in c.
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2015

transform_initialization = [];
dbg = false;
invert=false;
interpolation='linear';
centreOfRotation = [];
matrix_ = [];
ref = [];
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'interpolation'))
        interpolation=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    elseif (strcmp(varargin{i},'invert'))
        invert = true;
        i=i+1;
    elseif (strcmp(varargin{i},'ref'))
        ref = varargin{i+1};
        i=i+2;
    end
    
end

n = input_image.ndimensions;
if n==3
    matrix_use = rigidMatrixFromParameters([ c; c*0]); % this should be 4x4 ( or 3x3 for 2D which is not currently implemented)
elseif n==2
    matrix_use = rigidMatrixFromParameters([ c; 0]); % this should be 4x4 ( or 3x3 for 2D which is not currently implemented)
end

N_SPACE_COORDINATES = min(n,3);

imageMatrix = eye(N_SPACE_COORDINATES+1);
imageMatrix(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES) = input_image.orientation(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES);
imageMatrix(1:N_SPACE_COORDINATES,N_SPACE_COORDINATES+1) = input_image.origin(1:N_SPACE_COORDINATES);

CORMatrix = eye(N_SPACE_COORDINATES+1);

if numel(centreOfRotation)
    CORMatrix(1:N_SPACE_COORDINATES, N_SPACE_COORDINATES+1) = -centreOfRotation;
end

% Move the image to be centred about the centreofrotation and then after
% the rotation undo.
matrix_use = CORMatrix \  matrix_use  * CORMatrix;
if invert
    matrix_use = eye(N_SPACE_COORDINATES+1) / matrix_use;
end
orientation_matrix =   matrix_use \ imageMatrix ;

newOrigin = matrix_use\[input_image.origin(1:N_SPACE_COORDINATES) ; 1];
no = input_image.origin;
no(1:N_SPACE_COORDINATES)=newOrigin(1:N_SPACE_COORDINATES);

om = input_image.orientation;
om(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES) = orientation_matrix(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES);

out = ImageType(input_image.size,no,input_image.spacing,om);
out.data = input_image.data;
% then resample to the initial grid

if numel(ref)
    output_image = resampleImage(out, ref,'interpolation', interpolation);
else
    output_image = resampleImage(out, input_image,'interpolation', interpolation);
end

end


