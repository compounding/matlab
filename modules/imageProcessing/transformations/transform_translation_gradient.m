function grad_h = transform_translation_gradient(  x, params, varargin )
%TRANSFORM_TRANSLATION_GRADIENT Compute gradient of translation transformation
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2013

transform_initialization = [];
dbg = false;
reorder = true;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'no_reorder'))
        reorder = false;
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
    
end


NPARAMS = numel(params);


NDIMSO = transform_initialization.output_dimensionality;
noutputdimensions = NDIMSO;
grad_h = zeros(size(x,1)*noutputdimensions,NPARAMS);

%grad_h(:,0*NDIMSO + (1:NDIMSO) ) = g_tx(x, tx,ty,tz,a,b,c);
%grad_h(:,1*NDIMSO + (1:NDIMSO) ) = g_ty(x, tx,ty,tz,a,b,c);
%grad_h(:,2*NDIMSO + (1:NDIMSO) ) = g_tz(x, tx,ty,tz,a,b,c);

grad_h(:,1 ) = g_tx(x, params);
grad_h(:,2 ) = g_ty(x, params);
if numel(params)==3
    grad_h(:,3 ) = g_tz(x, params);
end


% grad_h has as many rows as input data points x, and as many columns as
% noutputdimension x nparams

end


function val = g_tx( x, params )
arr = zeros(size(params));
arr(1)=1;
val = repmat(arr,size(x,1),1);
end
%
function val = g_ty( x, params )
arr = zeros(size(params));
arr(2)=1;
val = repmat(arr,size(x,1),1);
end
%
function val = g_tz( x, params )
arr = zeros(size(params));
arr(3)=1;
val = repmat(arr,size(x,1),1);
end
