function [ output_image ] = transform_rigid(  input_image, c, varargin )
%TRANSFORM_RIGID  Transforms the input ND image using the rigid parameters in c
%   This function transforms the input image (ImageType) using the input rigid parameters specified in c. Angles are in radians!
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2015

transform_initialization = [];
dbg = false;
invert=false;
interpolation='linear';
centreOfRotation = [];
matrix_ = [];
adaptBounds = false;
inputAsPoints = false;
nopad= false;  % by default, consider only non-zero block. If set to true, consider all
noblur= false;
ref = [];
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'interpolation'))
        interpolation=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'centreOfRotation'))
        centreOfRotation=varargin{i+1};
        i=i+1;
      elseif (strcmp(varargin{i},'noPadding'))
        nopad=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'matrix'))
        matrix_=c;
        i=i+1;
    elseif (strcmp(varargin{i},'points'))
        inputAsPoints = true;
        inputPoints=input_image; % as row vector
        i=i+1;
    elseif (strcmp(varargin{i},'adaptBounds'))
        adaptBounds=true;
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    elseif (strcmp(varargin{i},'invert'))
        invert = true;
        i=i+1;
    elseif (strcmp(varargin{i},'noblur'))
        noblur= true;
        i=i+1;
    elseif (strcmp(varargin{i},'ref'))
        ref = varargin{i+1};
        i=i+2;
    end
    
end

if inputAsPoints
    n = size(inputPoints,2);
else
    n = input_image.ndimensions;
end

if n==4 && ~inputAsPoints
    output_image = ImageType(input_image);
    for i=1:input_image.size(end)
        im3D = input_image.extractFrame(i) ;
        out3D = transform_rigid(  im3D, c, varargin );
        output_image.data(:,:,:,i) = out3D.data;
    end
    return;
end

if ~numel(matrix_)
    matrix_use = rigidMatrixFromParameters(c); % this should be 4x4 ( or 3x3 for 2D which is not currently implemented)
    matrix_ = rigidMatrixFromParameters(c);
else
    matrix_use = matrix_;
end

if ~inputAsPoints
    
    N_SPACE_COORDINATES = min(n,3);
    
    imageMatrix = eye(N_SPACE_COORDINATES+1);
    imageMatrix(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES) = input_image.orientation(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES);
    imageMatrix(1:N_SPACE_COORDINATES,N_SPACE_COORDINATES+1) = input_image.origin(1:N_SPACE_COORDINATES);
    
    CORMatrix = eye(N_SPACE_COORDINATES+1);
    
    if numel(centreOfRotation)
        CORMatrix(1:N_SPACE_COORDINATES, N_SPACE_COORDINATES+1) = -centreOfRotation;
    end
    
    
    % Move the image to be centred about the centreofrotation and then after
    % the rotation undo.
    matrix_use = CORMatrix \  matrix_use  * CORMatrix;
    if invert
        matrix_use = eye(N_SPACE_COORDINATES+1) / matrix_use;
    end
    orientation_matrix =   matrix_use \ imageMatrix ;
    
    newOrigin = matrix_use\[input_image.origin(1:N_SPACE_COORDINATES) ; 1];
    no = input_image.origin;
    no(1:N_SPACE_COORDINATES)=newOrigin(1:N_SPACE_COORDINATES);
    
    om = input_image.orientation;
    om(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES) = orientation_matrix(1:N_SPACE_COORDINATES,1:N_SPACE_COORDINATES);
    
    out = ImageType(input_image.size,no,input_image.spacing,om);
    out.paddingValue = input_image.paddingValue;
    out.data = input_image.data;
    % then resample to the initial grid
    
    if numel(ref)
        if noblur
            output_image = resampleImage(out, ref,'interpolation', interpolation);
        else
            output_image = resampleImage(out, ref,'interpolation', interpolation,'blur',ones(out.ndimensions,1)*3,ones(out.ndimensions,1)*1);
        end
    elseif adaptBounds
        if nopad
            bounds = input_image.GetBounds(-1);
        else
            bounds = input_image.GetBounds(1);
        end
        base_string = [repmat('id',input_image.ndimensions,1) num2str((1:input_image.ndimensions)')];
        base_string2 = [num2str((1:2:(input_image.ndimensions*2))') repmat(':',input_image.ndimensions,1) num2str((2:2:(input_image.ndimensions*2))') repmat(',',input_image.ndimensions,1)];
        str1 = reshape([base_string repmat(',',input_image.ndimensions,1)]',1,[]);
        str2 = reshape([base_string repmat('(:) ',input_image.ndimensions,1)]',1,[]);
        str3 = reshape(base_string2',1,[]);
        
        eval([ '[' str1(1:end-1) ']=ndgrid(' str3(1:end-1)  ');']);
        eval([ 'ids = [' str2(1:end-1) '];']);
        
        bounds_warped = matrix_ \ [ bounds(ids) ones(size(ids,1),1)]';
        bounds_output(2:2:(input_image.ndimensions*2)) = max(bounds_warped(1:input_image.ndimensions,:)');
        bounds_output(1:2:(input_image.ndimensions*2)) = min(bounds_warped(1:input_image.ndimensions,:)');
        bounds_output = bounds_output(:);
        
        sp = input_image.spacing;
        sz = ceil((bounds_output(2:2:end) - bounds_output(1:2:end)-1)./sp);
        or = bounds_output(1:2:end);
        
        ref = ImageType(sz,or,sp,eye(n));
        if noblur
            output_image = resampleImage(out, ref,'interpolation', interpolation);
        else
            output_image = resampleImage(out, ref,'interpolation', interpolation,'blur',ones(out.ndimensions,1)*3,ones(out.ndimensions,1)*1);
        end
    else
        output_image = resampleImage(out, input_image,'interpolation', interpolation,'blur',ones(out.ndimensions,1)*3,ones(out.ndimensions,1)*1);
    end
    
else
    % points
    output_points = matrix_use * [inputPoints ones(size(inputPoints,1),1)]';
    output_image = output_points(1:n,:)';
end

end


