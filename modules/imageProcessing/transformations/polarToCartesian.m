function cartesianIm = polarToCartesian( im, resolution, varargin )
%% In radians



boundsPolar = im.GetBounds();

if numel(varargin)>0 && strcmp(varargin{1},'allcircle')
    xmin = -boundsPolar(end);
    xmax = boundsPolar(end);
    xbounds = [xmin xmax]';
    boundsCartesian = [xbounds(1); xbounds(2); xbounds(1); xbounds(2)];
else
    xmin  = sin(boundsPolar(1))*boundsPolar(end);
    xmax  = sin(boundsPolar(2))*boundsPolar(end);
    xbounds = [xmin xmax]';
    boundsCartesian = [xbounds(1); xbounds(2); boundsPolar([3 4]) ];
end


spacing = [resolution(1) resolution(2)]';

[xc, yc] = ndgrid(boundsCartesian(1):spacing(1):boundsCartesian(2), boundsCartesian(3):spacing(2):boundsCartesian(4));

a = atan2(xc,yc);
r = sqrt(xc.^2+yc.^2);

values = im.GetValue([a(:) r(:)]');

cartesianIm = ImageType(size(xc)', boundsCartesian([1 3]), spacing, eye(2));
cartesianIm.data(:) = values(:);
end
