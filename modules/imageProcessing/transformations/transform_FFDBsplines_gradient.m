function grad_h = transform_FFDBsplines_gradient(  x, params, varargin )
%TRANSFORM_FFDBSPLINES Summary of this function goes here
%   Detailed explanation goes here
% params are not needed but put in the argument for interface

transform_initialization = [];
dbg = false;
reorder = true;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'no_reorder'))
        reorder = false;
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
    
end

% FIXME: here the only grid we take into account is the small one. Needs to
% be modified so that all nodes are used.


rnth=-1;
grad_h = createBSplineSamplingMatrix(x,transform_initialization.grid.GetFullPatch(),...
                                    'bsd',transform_initialization.bsd,'noprogress',...
                                    'od',transform_initialization.output_dimensionality,...
                                    'remove_empty_nodes',rnth);

if 0
figure, 
hold on;
plotpoints2(x,'.')
plotpoints2(transform_initialization.grid,'*')
hold off;

end

                                
end

