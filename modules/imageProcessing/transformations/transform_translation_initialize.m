function transform_initialization = transform_translation_initialize(  input_image, transform_params, varargin )
%TRANSFORM_TRANSLATION_INITIALISE Initialise translation transformation
% 
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2013

% transform_params = [];

dbg = false;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
end

NPARAMS = input_image.ndimensions;

transform_params.bounds = input_image.GetBounds();
transform_initialization.output_dimensionality = numel(transform_params.bounds)/2;
transform_initialization.params0 = zeros(NPARAMS,1); 

end

