function [M,dM,dM2] = rigidMatrixFromParametersITKEuler(x,varargin)
% RIGIDMATRIXFROMPARAMETERS Computes the rigid matrix from the input rigid paramrters
% Data from the maxima script
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2013

c0 = [];

for i=1:numel(varargin)
   if strcmp(lower(varargin{i}),'centreoftransformation')
       c0 = varargin{i+1};
   end
end

if numel(x) == 6
    tx=x(1);
    ty=x(2);
    tz=x(3);
    a=x(4);
    b=x(5);
    c=x(6);
    
    Rx = [1 0 0 0;
        0 cos(a) -sin(a) 0;
        0 sin(a)  cos(a) 0
        0     0       0  1];
    
    Ry = [cos(b) 0 -sin(b) 0
        0       1      0  0
        sin(b)  0  cos(b) 0
        0       0      0  1];
    
    Rz = [cos(c) -sin(c) 0 0
        sin(c)  cos(c) 0 0
        0       0  1 0
        0       0  0 1];
    
    Dx = [1 0 0 tx
        0 1 0 0
        0 0 1 0
        0 0 0 1];
    Dy = [1 0 0 0
        0 1 0 ty
        0 0 1 0
        0 0 0 1];
    Dz = [1 0 0 0
        0 1 0 0
        0 0 1 tz
        0 0 0 1];
    
    M = Dx*Dy*Dz*Ry*Rx*Rz;
    
    CORMatrix = eye(3+1);
    if numel(c0)
        CORMatrix(1:3, 3+1) = -c0;
    end
    
    
    % Move the image to be centred about the centreofrotation and then after
    % the rotation undo.
    M = CORMatrix \  M * CORMatrix;
    
    
    %M = [cos(b)*cos(c)-sin(a)*sin(b)*sin(c) -cos(a)*sin(c) sin(a)*cos(b)*sin(c)+sin(b)*cos(c) tx
    %    cos(b)*sin(c)+sin(a)*sin(b)*cos(c) cos(a)*cos(c) sin(b)*sin(c)-sin(a)*cos(b)*cos(c) ty
    %    -cos(a)*sin(b) sin(a) cos(a)*cos(b) tz
    %    0 0 0 1];
    
    if nargout>1
        % compute the gradient
        dDx = [0 0 0 1
               0 0 0 0
               0 0 0 0
               0 0 0 0];
        dDy = [0 0 0 0
               0 0 0 1
               0 0 0 0
               0 0 0 0];
        dDz = [0 0 0 0
               0 0 0 0
               0 0 0 1
               0 0 0 0];
        
        dRx = [0 0 0 0;
            0 -sin(a) -cos(a)  0;
            0 cos(a)  -sin(a)  0
            0     0         0  0];
        dRy = [-sin(b) 0 -cos(b)  0
                0      0      0   0
               cos(b)  0  -sin(b) 0
                0      0      0   0];
        dRz = [-sin(c)  -cos(c) 0 0
                cos(c)  -sin(c) 0 0
                0       0       0 0
                0       0       0 0];
        
        dMx = dDx*Dy*Dz*Ry*Rx*Rz;
        dMy = Dx*dDy*Dz*Ry*Rx*Rz;
        dMz = Dx*Dy*dDz*Ry*Rx*Rz;
        dMa = Dx*Dy*Dz*Ry*dRx*Rz;
        dMb = Dx*Dy*Dz*Ry*Rx*dRz;
        dMc = Dx*Dy*Dz*dRy*Rx*Rz;
        
        dM = cat(3,dMx,dMy,dMz,dMa,dMb,dMc);
    end
    if nargout>2
        % compute the Jacobian
        % J = (Rz*Rx*Ry)';
        % And the gradient of the Jacobian, which is symply the
        % derivatives of the rotations
        dM2x = zeros(size(Dx))';
        dM2y = zeros(size(Dx))';
        dM2z = zeros(size(Dx))';
        dM2a = (Ry*dRx*Rz)';
        dM2b = (Ry*Rx*dRz)';
        dM2c = (dRy*Rx*Rz)';
        
        dM2 = cat(3,dM2x,dM2y,dM2z,dM2a,dM2b,dM2c);
        dM2 = dM2(1:end-1,1:end-1,:);
    end
    
elseif numel(x)==3
    tx=x(1);
    ty=x(2);
    a=x(3);
    
    Rz = [cos(a) -sin(a) 0;
        sin(a)  cos(a) 0
        0       0  1];
    Dx = [1 0 tx
        0 1 0
        0 0 1];
    Dy = [1 0 0
        0 1 ty
        0 0 1];
    
    M = Dx*Dy*Rz;
    
    CORMatrix = eye(2+1);
    if numel(c0)
        CORMatrix(1:2, 2+1) = -c0;
    end
    
    
    % Move the image to be centred about the centreofrotation and then after
    % the rotation undo.
    M = CORMatrix \  M * CORMatrix;
    
    if nargout>1
        
        dDx = [0 0 1
            0 0 0
            0 0 0 ];
        dDy = [0 0 0
            0 0 1
            0 0 0];
        
        dRz = [-sin(a) -cos(a) 0
            cos(a)  -sin(a) 0
            0       0  0];
        
        dMx = (dDx*Dy*Rz);
        dMy = (Dx*dDy*Rz);
        dMa = (Dx*Dy*dRz);
        
        
        dM = cat(3,dMx,dMy,dMa);
        
    end
    
    if nargout>2
        % compute the Jacobian
        % J = Rz';
        % And the gradient of the Jacobian, which is symply the
        % derivatives of the rotations
        dM2x = zeros(size(Dx));
        dM2y = zeros(size(Dy));
        dM2a = dRz;
        
        dM2 = cat(3,dM2x,dM2y,dM2a);
        dM2 = dM2(1:end-1,1:end-1,:);
    end
    
end

end