function [M,dM,dM2] = affineMatrixFromParameters(x)
% RIGIDMATRIXFROMPARAMETERS Computes the rigid matrix from the input rigid paramrters
% Data from the maxima script
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2013


if numel(x) == 12
    tx=x(1);
    ty=x(2);
    tz=x(3);
    a=x(4);
    b=x(5);
    c=x(6);
    sx = x(7);
    sy = x(8);
    sz = x(9);
    shx = x(10);
    shy = x(11);
    shz = x(12);
    
    Rx = [1 0 0 0;
        0 cos(a) -sin(a) 0;
        0 sin(a)  cos(a) 0
        0     0       0  1];
    
    Ry = [cos(b) 0 -sin(b) 0
        0       1      0  0
        sin(b)  0  cos(b) 0
        0       0      0  1];
    
    Rz = [cos(c) -sin(c) 0 0
        sin(c)  cos(c) 0 0
        0       0  1 0
        0       0  0 1];
    
    Dx = [1 0 0 tx
        0 1 0 0
        0 0 1 0
        0 0 0 1];
    Dy = [1 0 0 0
        0 1 0 ty
        0 0 1 0
        0 0 0 1];
    Dz = [1 0 0 0
        0 1 0 0
        0 0 1 tz
        0 0 0 1];
    Sx = eye(4); Sx(1,1)=sx;
    Sy = eye(4); Sx(2,2)=sy;
    Sz = eye(4); Sx(3,3)=sz;
    Shx = eye(4); Shx(2,1)=sx; Shx(1,2)=sx;
    Shy = eye(4); Shy(3,1)=sy; Shy(1,3)=sy;
    Shz = eye(4); Shz(3,2)=sz; Shz(2,3)=sz;
    
    M = Dx*Dy*Dz*Rz*Rx*Ry*Sx*Sy*Sz*Shx*Shy*Shz;
    
    
    if nargout>1
        %         % compute the gradient
        %         dDx = [0 0 0 1
        %                0 0 0 0
        %                0 0 0 0
        %                0 0 0 0];
        %         dDy = [0 0 0 0
        %                0 0 0 1
        %                0 0 0 0
        %                0 0 0 0];
        %         dDz = [0 0 0 0
        %                0 0 0 0
        %                0 0 0 1
        %                0 0 0 0];
        %
        %         dRx = [0 0 0 0;
        %             0 -sin(a) -cos(a)  0;
        %             0 cos(a)  -sin(a)  0
        %             0     0         0  0];
        %         dRy = [-sin(b) 0 -cos(b)  0
        %                 0      0      0   0
        %                cos(b)  0  -sin(b) 0
        %                 0      0      0   0];
        %         dRz = [-sin(c)  -cos(c) 0 0
        %                 cos(c)  -sin(c) 0 0
        %                 0       0       0 0
        %                 0       0       0 0];
        %
        %         dMx = dDx*Dy*Dz*Rz*Rx*Ry;
        %         dMy = Dx*dDy*Dz*Rz*Rx*Ry;
        %         dMz = Dx*Dy*dDz*Rz*Rx*Ry;
        %         dMa = Dx*Dy*Dz*Rz*dRx*Ry;
        %         dMb = Dx*Dy*Dz*Rz*Rx*dRy;
        %         dMc = Dx*Dy*Dz*dRz*Rx*Ry;
        %
        %         dM = cat(3,dMx,dMy,dMz,dMa,dMb,dMc);
    end
    if nargout>2
        %         % compute the Jacobian
        %         % J = (Rz*Rx*Ry)';
        %         % And the gradient of the Jacobian, which is symply the
        %         % derivatives of the rotations
        %         dM2x = zeros(size(Dx))';
        %         dM2y = zeros(size(Dx))';
        %         dM2z = zeros(size(Dx))';
        %         dM2a = (Rz*dRx*Ry)';
        %         dM2b = (Rz*Rx*dRy)';
        %         dM2c = (dRz*Rx*Ry)';
        %
        %         dM2 = cat(3,dM2x,dM2y,dM2z,dM2a,dM2b,dM2c);
        %         dM2 = dM2(1:end-1,1:end-1,:);
    end
    
elseif numel(x)==7
    tx=x(1);
    ty=x(2);
    a=x(3);
    sx=x(4);
    sy=x(5);
    shx=x(6);
    shy=x(7);
    
    Rz = [cos(a) -sin(a) 0;
        sin(a)  cos(a) 0
        0       0  1];
    Dx = [1 0 tx
        0 1 0
        0 0 1];
    Dy = [1 0 0
        0 1 ty
        0 0 1];
    Sx = [sx 0 0
        0 1 0
        0 0 1];
    Sy = [1 0 0
        0 sy 0
        0 0 1];
    Shx = [1 shx 0
           0 1 0
           0 0 1];
    Shy = [1 0 0
           shy 1 0
           0 0 1];
    M = Dx*Dy*Rz*Sx*Sy*Shx*Shy;
    
    if nargout>1
        
        %         dDx = [0 0 1
        %             0 0 0
        %             0 0 0 ];
        %         dDy = [0 0 0
        %             0 0 1
        %             0 0 0];
        %
        %         dRz = [-sin(a) -cos(a) 0
        %             cos(a)  -sin(a) 0
        %             0       0  0];
        %
        %         dMx = (dDx*Dy*Rz);
        %         dMy = (Dx*dDy*Rz);
        %         dMa = (Dx*Dy*dRz);
        %
        %
        %         dM = cat(3,dMx,dMy,dMa);
        
    end
    
    if nargout>2
        %         % compute the Jacobian
        %         % J = Rz';
        %         % And the gradient of the Jacobian, which is symply the
        %         % derivatives of the rotations
        %         dM2x = zeros(size(Dx));
        %         dM2y = zeros(size(Dy));
        %         dM2a = dRz;
        %
        %         dM2 = cat(3,dM2x,dM2y,dM2a);
        %         dM2 = dM2(1:end-1,1:end-1,:);
    end
    
end

end