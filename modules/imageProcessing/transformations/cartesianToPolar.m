function polarIm = cartesianToPolar( im_, alphabounds, alpharesolution,varargin)
% alpharesolution n1st angle, 2nd depth
% in radians
offsety =0;
if numel(varargin)
    offsety = varargin{1};
end


im = ImageType(im_);
im.data = im_.data;
im.origin(2) = im.origin(2)+offsety;


%%
boundsCartesian = im.GetBounds();
boundsPolar = [alphabounds(1); alphabounds(2); boundsCartesian([3 4])];
boundsPolar([1 3]) = boundsPolar([1 3]);
boundsPolar([2 4]) = boundsPolar([2 4]);
spacing_polar = [alpharesolution(1) alpharesolution(2)]';

[as, rs] = ndgrid(boundsPolar(1):spacing_polar(1):boundsPolar(2), boundsPolar(3):spacing_polar(2):boundsPolar(4));

xc = rs .* sin(as);
yc = rs .* cos(as);%-offsety;

values = im.GetValue([xc(:) yc(:)]','linear');


polarIm = ImageType(size(as)', boundsPolar([1 3]), spacing_polar, eye(2));
polarIm.data(:) = values(:);
end
