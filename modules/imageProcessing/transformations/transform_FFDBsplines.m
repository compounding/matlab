function [ output_image, positions, displacements_vector ] = transform_FFDBsplines(  input_image, c, varargin )
%TRANSFORM_FFDBSPLINES Summary of this function goes here
%   Detailed explanation goes here

transform_initialization = [];
dbg = false;
reorder = true;
interpolation='linear';
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'init'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'no_reorder'))
        reorder = false;
        i=i+1;
    elseif (strcmp(varargin{i},'interpolation'))
        interpolation=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    end
    
end


output_image = ImageType(input_image);
positions = output_image.GetPosition();
rnth = -1;  % do not remove any nodes

displacements = zeros(size(positions,2)*transform_initialization.output_dimensionality,1);

for l = 1:transform_initialization.nlevels
    
    B = createBSplineSamplingMatrix(positions',transform_initialization.grid(l),...
        'od',transform_initialization.output_dimensionality,...
        'bsd',transform_initialization.bsd,'reorder',reorder,...
        'noprogress','remove_empty_nodes',rnth);
    if ~iscell(c)
        displacements =  displacements + B*c;
    else
        displacements = displacements+ B*c{l};
    end
end

displacements_vector = reshape(displacements,transform_initialization.output_dimensionality,[]);

warped_positions = positions+displacements_vector;
values = input_image.GetValue(warped_positions,interpolation);

output_image.data(1:end)=values(1:end);

end

