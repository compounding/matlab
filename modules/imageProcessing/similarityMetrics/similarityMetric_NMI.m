function [value, value_gradient] = similarityMetric_NMI( source, target, transform_object, transform_params, varargin)
%SIMILARITYMETRIC_NMI [value, value_gradient] = similarityMetric_NMI( source, target, transform_function, transform_params)
%   Calculated the value and gradient of the normalized mutual information
%   between the warped source and target (both of type ImageType)
%
%   input arguments:
%       source - moving image to be warped (of type imageType)
%       target - fixed image(of type imageType)
%       transform_object - structure containing the following elements
%           transform_object.transform_function
%           transform_object.transform_gradient_function (optional)
%       transform_params - column vector with the transform parameters
%
% Author: Alberto Gomez, Biomedical Engineering, KCL, 2016



transform_initialization = [];
targetmask = [];
targetpad = [];
sourcemask=[];
sourcepad = [];
dbg = false;
reorder = true;
MAX_VALUE = 1;

reg_functions = {};

for i=1:size(varargin,2)
    if (strcmp(varargin{i},'transformInit'))
        transform_initialization=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'targetMask'))
        targetmask=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'targetPad'))
        targetpad=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'sourceMask'))
        sourcemask=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'sourcePad'))
        sourcepad=varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
        i=i+1;
    elseif (strcmp(varargin{i},'add_regularization'))
        n = numel(reg_functions);
        reg_functions{n+1}.reg_fun=varargin{i+1};
        reg_functions{n+1}.lambda=varargin{i+2};
        i=i+2;
    end
end


%% calculate metric value

T = double(target.data);

warped_source = transform_object.transform_function(source, transform_params);

% resample warped_source to target
if ( sum((warped_source.size-target.size).^2)>0 || ...
        sum((warped_source.origin-target.origin).^2)>0 ||...
        sum((warped_source.spacing-target.spacing).^2)>0)
    warped_source = resampleImage(warped_source, target,'interpolation','linear');
end

S = double(warped_source.data);

idx_toignore=[];
if numel(targetmask)
    idx_ = find(targetmask.data==0);
    idx_toignore = union(idx_toignore, idx_);
end

if numel(targetpad)
    idx_ = find(target.data<targetpad);
    idx_toignore = union(idx_toignore, idx_);
end
% DP missing sourcemask check
if numel(sourcepad)
    idx_ = find(warped_source.data<sourcepad);
    idx_toignore = union(idx_toignore, idx_);
end

idx_tokeep = setdiff(1:numel(T),idx_toignore);
idx_tokeep = idx_tokeep(:);

if ~numel(idx_tokeep)
   value = MAX_VALUE; 
   value_gradient = [];
   return;
end

%calculate NMI

% Compute normalized mutual information I(x,y)/sqrt(H(x)*H(y)) of two discrete variables x and y.
% Input:
%   x, y: two integer vector of the same length 
% Ouput:
%   z: normalized mutual information z=I(x,y)/sqrt(H(x)*H(y))
% Written by Mo Chen (sth4nth@gmail.com).
x = round(S(idx_tokeep));
y = round(T(idx_tokeep));
assert(numel(x) == numel(y));
n = numel(x);
x = reshape(x,1,n);
y = reshape(y,1,n);

l = min(min(x),min(y));
x = x-l+1;
y = y-l+1;
k = max(max(x),max(y));

idx = 1:n;
Mx = sparse(idx,x,1,n,k,n);
My = sparse(idx,y,1,n,k,n);
Pxy = nonzeros(Mx'*My/n); %joint distribution of x and y
Hxy = -dot(Pxy,log2(Pxy));


% hacking, to elimative the 0log0 issue
Px = nonzeros(mean(Mx,1));
Py = nonzeros(mean(My,1));

% entropy of Py and Px
Hx = -dot(Px,log2(Px));
Hy = -dot(Py,log2(Py));

% mutual information
MI = Hx + Hy - Hxy;

% normalized mutual information
z = sqrt((MI/Hx)*(MI/Hy));
z = max(0,z);


%-------

value = MAX_VALUE-z;

%numerator = (S(idx_tokeep) - mean(S(idx_tokeep)))'*(T(idx_tokeep) -mean(T(idx_tokeep) ));
%denominator = ( sqrt( (S(idx_tokeep) - mean(S(idx_tokeep)))'*(S(idx_tokeep) - mean(S(idx_tokeep)))) * ...
%    sqrt((T(idx_tokeep) -mean(T(idx_tokeep) ))'*(T(idx_tokeep) -mean(T(idx_tokeep) ))));
%value =   1-numerator/denominator;


%% calculate gradient values
value_gradient = [];
% N = numel(idx_tokeep);
% n = warped_source.ndimensions;
% 
% % nabla S
% out_ = gradientImage(warped_source);
% 
% out = reshape(out_,[prod(warped_source.size) n]);
% out = out(idx_tokeep,:);
% 
% % This is for the reordering
% totalrows = repmat(1:N,1,n); % This assumes that the vectorization of out goes starting by x
% totalcols = 1:n*N;
% totalcols = reshape(totalcols, n,[])';
% totalcols = totalcols(:)';
% grad_S = sparse(double(totalrows),double(totalcols) ,out(:)' ,N,n*N) ;
% 
% % nabla h
% x = target.GetPosition(idx_tokeep')';
% grad_h = transform_object.transform_gradient_function(x,transform_params);
% 
% % total gradient
% % gradient_metric = 2*(S(idx_tokeep)-T(idx_tokeep))'/numel(idx_tokeep);
% gradient_metric =  -(S(idx_tokeep) - mean(S(idx_tokeep)))'*(T(idx_tokeep) -mean(T(idx_tokeep) ))/...
%     ( sqrt( (S(idx_tokeep) - mean(S(idx_tokeep)))'*(S(idx_tokeep) - mean(S(idx_tokeep)))) * ...
%     sqrt((T(idx_tokeep) -mean(T(idx_tokeep) ))'*(T(idx_tokeep) -mean(T(idx_tokeep) ))));
% 
% 
% value_gradient = gradient_metric*grad_S * grad_h;
% for i=1:numel(reg_functions)
%     [value_r, grad_r ] = reg_functions{i}.reg_fun(transform_params);
%     value = value + reg_functions{i}.lambda*value_r;
%     value_gradient = value_gradient + reg_functions{i}.lambda*grad_r;
% end
% %

end

