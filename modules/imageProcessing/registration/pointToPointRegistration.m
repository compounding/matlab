function [T, residual] = pointToPointRegistration(source_points, target_points, varargin)
% Input points should be given in order of corespondance
% matched_points should be first column sources, second column targets

% TODO: check with GRAFTK!!!

weights = [];

for i=1:numel(varargin)
    if strcmp(lower(varargin{i}),'weights')
        weights=varargin{i+1};
    end
end

N = size(source_points,2);
npts = size(source_points,1);

if ~numel(weights)
    % if there is no weights, weight all points equally
    weights = ones(npts,1)*1/npts;
end

source_centroid = sum(source_points.*repmat(weights(:),1,N))./sum(weights);
target_centroid = sum(target_points.*repmat(weights(:),1,N))./sum(weights);

source_points_centred = source_points - ones(npts,1)*source_centroid;
target_points_centred = target_points - ones(npts,1)*target_centroid;
%%
H = zeros(N);
for i=1:npts
    sp = source_points_centred(i,:)*weights(i);
    tp = target_points_centred(i,:)*weights(i);
    H = H + sp'* tp;
end

[U, S, V] = svd(H);

%X = V * U';

C = eye(size(V));
%C(end) = det(U * V');
%rot = U * C * V';
C(end) = det(V * U');
rot = V * C * U';
a = trace(H'*rot) / trace(target_points_centred * target_points_centred'); % scale


%trans = target_centroid' - a*rot * source_centroid';
trans = target_centroid' - rot * source_centroid';

T.rot = rot;
T.translation = trans;
T.a = a;


%offsets = a * rot * source_points' + (T.translation*ones(1,npts)) - target_points';
offsets =  rot * source_points' + (T.translation*ones(1,npts)) - target_points';
distances = sqrt(sum(offsets.^2));
residual = mean(distances);


end
