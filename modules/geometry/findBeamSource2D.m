function [beamSource_wc, angles] = findBeamSource2D(im)
usedlength = round(im.size(2)*0.15):round(im.size(2)*0.55);
%usedlength = 1:im.size(2);
ch = bwconvhull(im.data>0);
BW = edge(ch,'canny');
BW = BW(:,usedlength);
[H,T,R] = hough(BW,'RhoResolution',0.5,'ThetaResolution',0.5);
P  = houghpeaks(H,2,'threshold',ceil(0.5*max(H(:))));

% angles
angles_(1) = T(P(1,2));
angles_(2) = T(P(2,2));

Up = [-sind(angles_(1)) cosd(angles_(1)) ];
Uq = [-sind(angles_(2)) cosd(angles_(2)) ];

radius_(1) = R(P(1,1));
radius_(2) = R(P(2,1));

% this has been verfied
p1 = (Up([2 1]).*[-1 1])*radius_(1);
q1 = (Uq([2 1]).*[-1 1])*radius_(2);

%lines = houghlines(BW,T,R,P,'FillGap',5,'MinLength',numel(usedlength)/7);
%Up = lines(1).point2(:)-lines(1).point1(:);
%Uq = lines(2).point2(:)-lines(2).point1(:);
%Up = Up/norm(Up);
%Uq = Uq/norm(Uq);
%p1 = lines(1).point1;
%q1 = lines(2).point2;
beamSource = -intersectionLineLine(Up(:),p1(:),Uq(:),q1(:))+[usedlength(1)-1 0]';
% transform the point to world coordinates
beamSource_wc = (beamSource([2 1])-1).*im.spacing+im.origin;

% v1 = Up(:).*im.spacing([2 1]);
% v2 = Uq(:).*im.spacing([2 1]);
% v1 = v1/norm(v1);
% v2 = v2/norm(v2);
% 
% angles_(1) = atan2d(v1(1),v1(2));
% angles_(2) = atan2d(v2(1),v2(2));
% 
angles(1)=min(angles_);
angles(2)=max(angles_);

if angles(2)>90
    angles = angles-90;
end

if angles(1)<-90
    angles = angles+90;
end

end