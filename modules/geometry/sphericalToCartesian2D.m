function cartesianIm = sphericalToCartesian2D( im, resolution )


%%
boundsSpherical = im.GetBounds();

xmin  = sind(boundsSpherical(1))*boundsSpherical(end);
xmax  = sind(boundsSpherical(2))*boundsSpherical(end);

xbounds = [xmin xmax]';

boundsCartesian = [xbounds(1); xbounds(2); boundsSpherical([3 4]) ];
spacing = [resolution(1) resolution(2)]';

[xc, yc] = ndgrid(boundsCartesian(1):spacing(1):boundsCartesian(2), boundsCartesian(3):spacing(2):boundsCartesian(4));

a = atan2d(xc,yc);
r = sqrt(xc.^2+yc.^2);

values = im.GetValue([a(:) r(:)]','linear');

cartesianIm = ImageType(size(xc)', boundsCartesian([1 3]), spacing, eye(2));
cartesianIm.data(:) = values(:);
end