function [sphericalIm, cartesian_positions] = cartesianToSpherical2D( im, alphabounds, alpharesolution )
% alpharesolution n1st angle, 2nd depth

%%
boundsCartesian = im.GetBounds();
boundsSpherical = [alphabounds(1); alphabounds(2); boundsCartesian([3 4])];
boundsSpherical([1 3]) = boundsSpherical([1 3]);
boundsSpherical([2 4]) = boundsSpherical([2 4]);
spacing_spherical = [alpharesolution(1) alpharesolution(2)]';

[xs, ys] = ndgrid(boundsSpherical(1):spacing_spherical(1):boundsSpherical(2), boundsSpherical(3):spacing_spherical(2):boundsSpherical(4));

xc = ys .* sind(xs);
yc = ys .* cosd(xs);%-offsety;

values = im.GetValue([xc(:) yc(:)]','linear');


sphericalIm = ImageType(size(xs)', boundsSpherical([1 3]), spacing_spherical, eye(2));
sphericalIm.data(:) = values(:);

cartesian_positions = [xc(:) yc(:)]';
end