# Medical Image Processing Toolbox

By Alberto Gomez (alberto.gomez@kcl.ac.uk)

This package contains a collection of classes and functions, which allow to comfortably work with medical images and meshes. It provides a intuitive and transparent way of dealing with spacing, origin, image orientation, etc. 

The package includes functions for input-output with common image formats (mhd, gipl) and mesh formats (vtk, stl), and basic (although not necessarily simple) image processing operations such as image resample, image reslice and image transform.

Please do not hesitate to propose new features, examples or any constructive comments! 

The matlab file exchange link can be found <a href="https://uk.mathworks.com/matlabcentral/fileexchange/41594-medical-image-processing-toolbox">here</a>.

If you use this work, please cite:

*[Gomez, Alberto. "MIPROT: A Medical Image Processing Toolbox for MATLAB"	arXiv preprint arXiv:2104.04771 (2021)](https://arxiv.org/abs/2104.04771)*

# License
Copyright (c) 2019, Alberto Gomez
Copyright (c) 2010, Dirk-Jan Kroon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution
* Neither the name of King's College of London nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
