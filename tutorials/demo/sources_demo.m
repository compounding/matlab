% Sources demo
% box
center = [0 0 0]';
dims = [10 20 5]';
b = boxMesh(center, dims);
subplot(1, 5, 1)
viewMesh(b);
view(30, 30);
axis equal
title('boxMesh')

% cylinder
ax = [1 0 0]';
radius = 5;
height = 20;
resolution = 12;
b = cylinderMesh(ax,center,radius, height,resolution);
subplot(1, 5, 2)
viewMesh(b);
view(30, 30);
axis equal
title('cylinderMesh')

% ellipsoid
radius = [10 20 5]';
b = ellipsoidMesh(center,radius, 'resolution', 20);
subplot(1, 5, 3)
viewMesh(b);
view(30, 30);
axis equal
title('ellipsoidMesh')


% plane
point = [2 0 0]'; 
normalVector = [0 0 1]';
b = planeMesh(point, normalVector, 'scale', 2);
subplot(1, 5, 4)
viewMesh(b);
view(30, 30);
axis equal
title('planeMesh')

% sphere
radius = 10;
b = sphereMesh(center,radius, 'resolution', 20);
subplot(1, 5, 5)
viewMesh(b);
view(30, 30);
axis equal
title('sphereMesh')


