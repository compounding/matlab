%fixed = read_mhd([getenv('HOME') '/repos/compounding/matlab/tutorials/data/fixed.mhd']);
%moving= read_mhd([getenv('HOME') '/repos/compounding/matlab/tutorials/data/moving.mhd']);
fixed = read_picture([getenv('HOME') '/repos/compounding/matlab/tutorials/data/c1.png']);
moving= read_picture([getenv('HOME') '/repos/compounding/matlab/tutorials/data/c3.png']);

fixed = resampleImage(fixed, [], 'spacing',fixed.spacing.*[3 4]');
moving= resampleImage(moving, [], 'spacing',moving.spacing.*[3 4]');

%% Display images initially
difference = ImageType(fixed);
difference.data = fixed.data - moving.data;
figure;
subplot(2,3,1); fixed.show(); axis equal; axis off; title('Fixed')
subplot(2,3,2); moving.show(); axis equal; axis off; title('Moving')
subplot(2,3,3); difference.show(); axis equal; axis off; title('Difference before reg.')

%% Set up the registration process.

transform_params.nlevels=1;
transform_params.bsd=1;
transform_params.bounds=-1; % if -1 then take from image
transform_params.grid_spacing = [10 10]';

transform_initialization = transform_FFDBsplines_initialize(  moving, transform_params);

x0 = randn(size(transform_initialization.params0{1}))*10;

options = optimset('GradObj','off', 'Display', 'iter', 'MaxIter', 100);
transform_object.transform_function = @(moving, params) transform_FFDBsplines(moving, params, 'init', transform_initialization);

f = @(x) similarityMetric_NCC( moving, fixed, transform_object, x);

%% Run registration

X = fminlbfgs(f, x0, options);
%% Plots
moving_tx = transform_FFDBsplines(moving, X, 'init', transform_initialization);
difference.data = fixed.data - moving_tx.data;
subplot(2,3,5); moving_tx.show(); axis equal; axis off; title('Moving after reg')
subplot(2,3,6); difference.show(); axis equal; axis off; title('Difference after reg.')