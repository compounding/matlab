fixed = read_mhd([getenv('HOME') '/repos/compounding/matlab/tutorials/data/fixed.mhd']);
moving= read_mhd([getenv('HOME') '/repos/compounding/matlab/tutorials/data/moving.mhd']);

%% Add some random displacement to the moving image
moving = transform_rigid(moving, [0.0 0.0 15*pi/180], 'ref', moving);

%% Display images initially
difference = ImageType(fixed);
difference.data = fixed.data - moving.data;
figure;
subplot(2,3,1); fixed.show(); axis equal; axis off; title('Fixed')
subplot(2,3,2); moving.show(); axis equal; axis off; title('Moving')
subplot(2,3,3); difference.show(); axis equal; axis off; title('Difference before reg.')

%% Set up the registration process.
x0 = [0 0 0];

options = optimset('GradObj','off', 'Display', 'iter');
transform_object.transform_function = @(moving, params) transform_rigid(moving, params, 'ref', fixed);

f = @(x) similarityMetric_NCC( moving, fixed, transform_object, x);

%% Run registration

X = fminlbfgs(f, x0, options);

moving_tx = transform_rigid(moving, X, 'ref', fixed);
difference.data = fixed.data - moving_tx.data;
subplot(2,3,5); moving_tx.show(); axis equal; axis off; title('Moving after reg')
subplot(2,3,6); difference.show(); axis equal; axis off; title('Difference after reg.')