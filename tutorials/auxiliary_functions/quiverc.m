function h = quiverc(x,y,vx,vy,scale,scalar, cm,m,M)
%QUIVERC coloured quiver
%   plot 2D vectors coloured by a scalar value
%
%   quiverc(x,y,vx,vy,scale,scalar, cm,m,M)
%       scale=0 -> vectors without scaling

Ncolors = size(cm,1);
cm(round(scalar/max(scalar)*(Ncolors-1)+1),:);
hold on;
for i=1:Ncolors
    minv = (M-m)/Ncolors * (i-1) +m;
    maxv = (M-m)/Ncolors * (i) + m + eps;
    ids = find(scalar>=minv & scalar <maxv);
    h(i) = quiver(x(ids), y(ids), vx(ids), vy(ids),scale,'Color',cm(i,:));
end
hold off;

end