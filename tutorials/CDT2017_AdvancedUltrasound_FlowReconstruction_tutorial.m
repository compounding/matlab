%% 2D Flow reconstruction from Doppler
% The aim of this tutorial is to reconstruct 2D cardiac flow patterns from
% 2D colour Doppler ultrasound images acquired on a fetal subject.
% The processing pipeline is based on the paper:
%
% Garcia, Damien, et al. "Two-dimensional intraventricular flow mapping by
% digital processing conventional color-Doppler echocardiography images."
% IEEE transactions on medical imaging 29.10 (2010): 1701-1713.
clear; clc;
%% Algorithmic parameters
doppler_blurring = 2; % standard deviation (in pixels) of the Gaussian kernel used for blurring. Must be a number >= 1
spacing_polar = [0.25*pi/180 0.5]; % pixel size of data in polar coordinates: resollution in the angle direction (in radians) and depth (in mm)
angles_range = [-25 25]*pi/180; % Ultrasound frustum angles (in radians)

reconstruct_right_ventricle = true; % if false, the right ventricle will be reconstructed

%% Data loading
% Load the BMode and the colour Doppler images
folder = '/home/ag09/Dropbox/MRes Medical Image Computing 2017-18/Tutorials/Doppler Flow Reconstruction/data'; 
bmode = read_mhd([folder '/bmode.mhd']);
doppler = read_mhd([folder '/doppler.mhd']);

%% Create masks for ventrcle

%Use SimpleViewer_GUI on the bmode image
% e.g. 
% >> SimpleViewer_GUI(bmode)

% Read the masks that have been created using read_mhd.m
LVmask = read_mhd('/home/ag09/repos/compounding/matlab/tutorials/data/LV2.mhd'); %  TO BE COMPLETED
RVmask = read_mhd('/home/ag09/repos/compounding/matlab/tutorials/data/RV2.mhd'); % TO BE COMPLETED

if reconstruct_right_ventricle
    mask = RVmask;
else
    mask = LVmask;
end

% Create a full heart mask to mask out spurious doppler data.
doppler.data = doppler.data.*mask.data;

%% Convert data to polar coordinates

bmode_polar = cartesianToPolar(bmode,angles_range,spacing_polar);
doppler_polar = cartesianToPolar(doppler,angles_range,spacing_polar);
mask_polar =  cartesianToPolar(mask,angles_range,spacing_polar);

%% Data pre-processing
% Filter (blur) doppler data a bit
doppler_polar_filtered = ImageType(doppler_polar,'copy');
doppler_polar_filtered.data  = imgaussfilt(doppler_polar_filtered.data,doppler_blurring);

% We negate the radial velocity because positive means towards the
% transducer, but depth goes positive away from the transducer.
Vr = -doppler_polar_filtered.data ; % TO BE COMPLETED
% Estimate the gradient as centred differences:
%  dVr =  ( Vr(i-1)-Vr(i+1))/(2*Ar)
dVr = (Vr(:,[1 1:end-1] ) - Vr(:,[2:end end] ))/(2*doppler_polar.spacing(2)); % TO BE COMPLETED
pts_theta_rad = doppler_polar_filtered.GetPosition();

t = reshape(pts_theta_rad(1,:),size(dVr));
r = reshape(pts_theta_rad(2,:),size(dVr));

dVt = -r.*dVr-Vr; % TO BE COMPLETED
dVt(dVt~=dVt)=0;
% Estimate integrals through discrete sums
w_spacing = ones(size(Vr))*spacing_polar(1);
Vm = cumsum(dVt.*w_spacing); % left to right integral
Vp = flipud(cumsum(dVt(end:-1:1,:).*w_spacing)); % right to left

% standard weights (sub-optimal)
%wp = repmat(linspace(0,1,size(Vm,1)),size(Vm,2),1).^2';
% improved weights (as described in the paper)
wp = cumsum((dVt - repmat(min(dVt),size(dVt,1),1)).*w_spacing)./repmat(sum((dVt - repmat(min(dVt),size(dVt,1),1)).*w_spacing),size(dVt,1),1);
wm = 1-wp;  % TO BE COMPLETED

% Weighted sum to find out the angular component of the velocity
Vt = (wm.*Vm+wp.*Vp);

% Convert to cartesian coordinates. 
px = r.*sin(t); 
py = r.*cos(t);

uRx = px./sqrt(px.^2+py.^2); % radial unit vector for each point - x
uRy = py./sqrt(px.^2+py.^2); % radial unit vector for each point - y
uTx = -uRy; % angular unit vector for each point - x
uTy = uRx; % angular unit vector for each point - y


% convert velocities to cartesian coordinates
vx = (Vr.*uRx + Vt.*uTx).*mask_polar.data;
vy = (Vr.*uRy + Vt.*uTy).*mask_polar.data;

%% Display results
toremove = isnan(vx);
px(toremove)=[];
py(toremove)=[];
vx(toremove)=[];
vy(toremove)=[];
venc = max(abs(doppler.data(:)));
vnorm = sqrt(vx(:).^2+vy(:).^2);
vector_magnitude_factor = 1/100;

figure
bmode.show();
hold on;
doppler.show('opacity',0.5,'ColorRange',[-venc venc],'colorMap',dopplerColors);
colormap(dopplerColors)
colorbar
hold off;
hold on;
%quiver(px(:), py(:),vx(:),vy(:)); % plot with normal vectors
quiverc(px(:), py(:),vx(:)*vector_magnitude_factor,vy(:)*vector_magnitude_factor,0,vnorm, jet,0, max(vnorm)); % plot with coloured vectors
hold off;
title('2D vector reconstruction from Garcia et al., 2010')
axis equal;
axis(bmode.GetBounds);
