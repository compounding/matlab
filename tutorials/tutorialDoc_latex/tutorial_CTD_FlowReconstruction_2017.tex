% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{scrartcl} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{filecontents}

\begin{filecontents}{jobname.bib}

@article{Garcia2010,
  title={Two-dimensional intraventricular flow mapping by digital processing conventional color-Doppler echocardiography images},
  author={Garcia, Damien and del {\'A}lamo, Juan C and Tann{\'e}, David and Yotti, Raquel and Cortina, Cristina and Bertrand, {\'E}ric and Antoranz, Jos{\'e} Carlos and P{\'e}rez-David, Esther and Rieu, R{\'e}gis and Fern{\'a}ndez-Avil{\'e}s, Francisco and others},
  journal={IEEE transactions on medical imaging},
  volume={29},
  number={10},
  pages={1701--1713},
  year={2010},
  publisher={IEEE}
}
\end{filecontents}

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

% For code
\usepackage{color} % defines a new color
\definecolor{SolutionColor}{rgb}{0.8,0.9,1} % light blue
\usepackage{graphicx,url}
\usepackage{listings}






\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}


\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Octave,                 % the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=5,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
%  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

%%% END Article customizations

%%% The "real" document content comes below...

\title{2D Blood Flow Reconstruction from Colour Doppler Ultrasound}
\subtitle{CDT Course - Medical Image Computing}
\author{Alberto Gomez - alberto.gomez@kcl.ac.uk}
\date{December 5, 2017} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 


\begin{document}
\maketitle

\section{Introduction}

Colour Doppler images, such as the image in Fig. \ref{fig:doppler}, provide blood flow measurements over a 2D region. One limitation of Doppler ultrasound is that only the velocities along the beam direction are captured. The magnitude of this component is colour-coded from blue (negative) to red (positive) as shown in the figure. The convention is that positive velocities go towards the transducer (in the bottom of the figure in this case) and that negative velocities go away from the transducer.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/doppler}
\caption{2D colour Doppler image of a fetal heart.\label{fig:doppler}}
\end{figure}


In this tutorial we will implement a simplified version of the 2D flow reconstruction method described in \cite{Garcia2010}. This method allows to recover all components of the velocity from a colour Doppler image. The paper \cite{Garcia2010} is provided for reading prior to the class, and a summary of the main aspects of the method can be found below. The implementation will be done using MATLAB; please make sure that you have matlab installed in your laptop, preferably with the latest version (R2017b). Read carefully and entirely this document before you start working on the implementation.

In summary, the method consists of the following steps:
\begin{enumerate}
	\item Find the ventricular masks (i.e. segment the domain where the flow will be reconstructed).
	\item Convert the images (and the masks) to polar coordinates. In this domain, the known velocities run parallel to the radial direction, and the component to be found is the angular one.
	\item Estimate the radial velocity gradient.
	\item Solve the equations in \cite{Garcia2010}.
	\item Compute the weights to combine the left and the right angular velocities computed.
	\item transform the velocity vectors to Cartesian and display the result.
\end{enumerate}

\section{Tutorial}

The tasks are the following:
\begin{enumerate}
	\item Complete the sections marked as ``TO BE COMPLETED'' in the matlab file.
	\item Vary the algorithm parameters and observe the effects on the reconstructed flow.
\end{enumerate} 

The following sections will guide you step by step to complete the tutorial.

\subsection{Environment set-up}

Download all the files provided for this tutorial into a folder \texttt{<folder>} and add all folders and subfolders to your matlab path. the \texttt{<folder>/data} subfolder should contain two images (4 files in total) with the background b-mode and the doppler data.

\subsection{Segment the ventricular masks}

A matlab app to carry out visualization and segmentation within matlab called \texttt{SimpleViewer\_GUI.m} is provided within the \texttt{<folder>/viewer} subfolder. To create the masks for the right ventricle (RV) and the left ventricle (LV), do the following:
\begin{figure}[!htb]
\subfloat[\label{fig:seg1}]{
	\includegraphics[width=0.45\textwidth]{figures/seg1}
	}
	\subfloat[\label{fig:seg2}]{
	\includegraphics[width=0.45\textwidth]{figures/seg2}
	}
	
	\subfloat[\label{fig:seg3}]{
	\includegraphics[width=0.45\textwidth]{figures/seg3}
	}
	\subfloat[\label{fig:seg4}]{
	\includegraphics[width=0.45\textwidth]{figures/seg4}
	}
\caption{Image segmentation with the SimpleViewer\_{GUI}.\label{fig:doppler}}
\end{figure}
\begin{enumerate}
	\item Launch the app with the bmode image:
	\begin{verbatim}
>> bmode = read_mhd([folder '/data/bmode.mhd']);
>> SimpleViewer_GUI(bmode);
	\end{verbatim}
	\item Select the \texttt{Tools -> mask -> Polygon} option in the menu (Fig. \ref{fig:seg1}).
	\item Click points around the contour of the RV (Fig. \ref{fig:seg2}).
	\item Double click the last point to close the shape (Fig. \ref{fig:seg3}).
	\item Save the mask using the \texttt{File -> save -> save image -> foreground} option in the menu (Fig. \ref{fig:seg3}).
	\item Repeat for the LV.
\end{enumerate} 

\subsection{Convert images to polar coordinates}

The functions for this are provided, and the code goes as follows:

\begin{verbatim}
bmode_polar = ...
    cartesianToPolar2D(bmode,angles_range,spacing_polar);
doppler_polar = ...
    cartesianToPolar2D(doppler,angles_range,spacing_polar);
mask_polar =  ...
    cartesianToPolar2D(mask,angles_range,spacing_polar);
\end{verbatim}

if you want to visualize the polar images, you can with the SimpleViewer\_{GUI} by doing:

\begin{verbatim}
>> SimpleViewer_GUI(bmode_polar,doppler_polar,'polar')	
\end{verbatim}

\subsection{Data preprocessing}

It is normally a good idea to blur the Doppler image a bit to remove noise. This is done with the lines:
\begin{verbatim}
doppler_polar_filtered = ImageType(doppler_polar,'copy');
doppler_polar_filtered.data = ...
    imgaussfilt(doppler_polar_filtered.data,doppler_blurring);
\end{verbatim}

\subsection{Compute the missing component of the velocity}

The Doppler image in polar coordinates provides the radial velocity, $V_{r}(r,\theta)$. The method calculates the missing component $V_{\theta}(r,\theta)$ with the continuity equation (mass conservation) and  using the wall motion as boundary condition. in this tutorial we use the simplification that the wall does not move, therefore wall velocity $V_{\theta^{-}} = 0$ and $V_{\theta^{+}} = 0$. As a result the equation to solve is:

\begin{equation}
	V_{\theta} = w \int^{\theta}_{-\infty} \partial_{\theta} V_{\theta}  d\theta - (1-w) \int^{\infty}_{\theta} \partial_{\theta} V_{\theta}  d\theta{}
	\label{eq:vel1}
\end{equation}
For which the derivative of the radial velocity is computed as $\partial_{\theta} V_{\theta} = -r dV_{r} -V_{r}$. In this tutorial we will approximate the integrals in (\ref{eq:vel1}) with discrete sums.


\subsection{Convert the reconstructed velocity to Cartesian coordinates}

For the conversion, we use the relation
\begin{equation}
	\begin{array}{ccc}
		V_x &=& V_{r} \hat{r}_{x} + V_{\theta} \hat{\theta}_{x} \\
		V_y &=& V_{r} \hat{r}_{y} + V_{\theta} \hat{\theta}_{y}
	\end{array}
\end{equation}
where $(\hat{r}_{x}, \hat{r}_{y})$ and $(\hat{\theta}_{x}, \hat{\theta}_{y})$ are the local polar basis in Cartesian coordinates.

\subsection {Visualize}

Visualization is provided, using the following code snippet:

\begin{verbatim}
venc = max(abs(doppler.data(:)));
vnorm = sqrt(vx(:).^2+vy(:).^2);
vector_magnitude_factor = 1/100;

figure
bmode.show();
hold on;
doppler.show('opacity',0.5,'ColorRange',[-venc venc],'colorMap',dopplerColors);
colormap(dopplerColors)
colorbar
hold off;
hold on;
%quiver(px(:), py(:),vx(:),vy(:)); % plot with normal vectors
quiverc(px(:), py(:),vx(:)*vector_magnitude_factor,vy(:)*vector_magnitude_factor,0,vnorm, jet,0, max(vnorm)); % plot with coloured vectors
hold off;
title('2D vector reconstruction from Garcia et al., 2010')
axis equal;
axis(bmode.GetBounds);

\end{verbatim}

%Implement the function, from the template provided. You can calculate the FLE from the FRE.
%\lstinputlisting[language=Matlab]{../CDT2017_AdvancedUltrasound_FlowReconstruction_tutorial.m}
%\lstinputlisting[language=Matlab]{solution/calculate_FLE.m}






\bibliographystyle{plain}
\bibliography{jobname} 


\end{document}
