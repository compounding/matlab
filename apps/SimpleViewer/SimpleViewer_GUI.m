function varargout = SimpleViewer_GUI(varargin)
% SIMPLEVIEWER_GUI MATLAB code for SimpleViewer_GUI.fig
%      SIMPLEVIEWER_GUI, by itself, creates a new SIMPLEVIEWER_GUI or raises the existing
%      singleton*.
%
%      H = SIMPLEVIEWER_GUI returns the handle to a new SIMPLEVIEWER_GUI or the handle to
%      the existing singleton*.
%
%      SIMPLEVIEWER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMPLEVIEWER_GUI.M with the given input arguments.
%
%      SIMPLEVIEWER_GUI('Property','Value',...) creates a new SIMPLEVIEWER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SimpleViewer_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SimpleViewer_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
%  By Alberto Gomez (c) 2014 - King's Collee London -  alberto.gomez@kcl.ac.uk
%
% See also: GUIDE, GUIDATA, GUIHANDLES



% Last Modified by GUIDE v2.5 29-Nov-2017 10:47:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SimpleViewer_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @SimpleViewer_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimpleViewer_GUI is made visible.
function SimpleViewer_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SimpleViewer_GUI (see VARARGIN)
% ------------------------------------------------------------------------

%------------------------------------------------------------------------
% Choose default command line output for SimpleViewer_GUI
handles.output = hObject;

enable3D = false;

% general parameters
colors(1,:) = [1 0 0];
colors(2,:) = [0 0 1];
colors(3,:) = [0 0.5 0 ];

Mslice{1}=[1 0 0
    0 -1 0
    0 0 -1]';
Mslice{2}=[1 0 0
    0 0 1
    0 -1 0]';
Mslice{3}=[0 1 0
    0 0 1
    1 0 0]';

RotZ = eye(3);
RotY = eye(3);
RotX = eye(3);
CurrentAxisMatrix = eye(3);
handles.CurrentAxisMatrix = CurrentAxisMatrix;
handles.colors = colors;
handles.currentFrame =1;
handles.Mslice = Mslice;

factor=0.5;
% setup plots
axis_h = zeros(3,1);

% for registration

handles.regmatrix = eye(4);
handles.regparams = zeros(6,1);
handles.regparams_s = zeros(6,1);
handles.regparams_s(6)=60;
handles.lastParamsToBeUsed = 0;

axis_h(1)=handles.axes1;
axis_h(2)=handles.axes2;
axis_h(3)=handles.axes3;
axis_h(4)=handles.axes4;
handles.axis_h = axis_h;
handles.pause_loop = false;
guidata(hObject, handles);

handles.opacity = 0.5;

handles.factor=factor;
handles.enable3D=enable3D;
handles.Rot{1} = RotZ;
handles.Rot{2} = RotY;
handles.Rot{3} = RotX;
handles.n_images = [0 0];

% default colormaps

handles.colormap{1}=gray;
handles.colormap{2}=dopplerColors;
handles.overly_th = -1;



% default window limits
handles.windowLimits{1}=[0 255];
handles.windowLimits{2}=[0 255];
% Update handles structure
guidata(hObject, handles);

% input data: start with a all-black image----------------------------------------------
handles.backgroundImageFiles =[];
handles.currentImageFile =1;
handles.maskImageFiles =[];
im = PatchType([10 10 10]',[0 0 0]',[1 1 1]',eye(3));
if numel(varargin)>0
    do_continue = true;
    % first argument is an image
    if isa(varargin{1},'ImageType') || isa(varargin{1},'PatchType')
        im = varargin{1};
    elseif isa(varargin{1},'cell') &&  isa(varargin{1}{1},'char')
        handles.backgroundImageFiles = varargin{1};
        im = read_mhd(handles.backgroundImageFiles{handles.currentImageFile});
    elseif isa(varargin{1},'double')
        data = varargin{1};
        sz = size(data)';
        sp = sz*0+1;
        ori = -(sz-1)/2.*sp;
        im = PatchType(sz,ori,sp,eye(numel(sz)));
        im.data = data;
    else
        do_continue = false;
    end
    
    if do_continue
        handles.windowLimits{1}=[min(im.data(:)) max(im.data(:))];
        
        if im.ndimensions ==2
            % convert the image to 3D to avoid problems
            M = eye(3);
            M(1:2,1:2) = im.orientation;
            im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
            im2.data = repmat(im.data,1,1,2);
            im = im2;
        end
    end
    
end



handles.im{1} = im;
handles.n_images = [1 0];

if numel(varargin)>1
    do_continue = true;
    % second argument as overlay
    if isa(varargin{2},'ImageType') || isa(varargin{2},'PatchType')
        im2 = varargin{2};
    elseif isa(varargin{2},'cell') &&  isa(varargin{2}{1},'char')
        handles.maskImageFiles = varargin{2};
        im2 = read_mhd(handles.maskImageFiles{handles.currentImageFile});
    elseif isa(varargin{2},'double')
        data = varargin{2};
        sz = size(data)';
        sp = sz*0+1;
        ori = -(sz-1)/2.*sp;
        im2 = PatchType(sz,ori,sp,eye(numel(sz)));
        im2.data = data;
    else
        do_continue = false;
    end
    if do_continue
        if im2.ndimensions ==2
            % convert the image to 3D to avoid problems
            M = eye(3);
            M(1:2,1:2) = im2.orientation;
            im3 = PatchType([im2.size; 2], [im2.origin ; -0.5], [im2.spacing ; 1], M);
            im3.data = repmat(im2.data,1,1,2);
            im2 = im3;
        end
        
        im2 = resampleImage(im2,handles.im{1},'in_gui');
        handles.im{2} = im2;
        handles.windowLimits{2}=[-max(abs(im2.data(:))) max(abs(im2.data(:)))];
        set(handles.checkbox_overlay,'Enable','on') ; %set to off
        val = get(handles.checkbox_overlay,'Max');
        set(handles.checkbox_overlay,'Value',val);  % uncheck
        handles.n_images = [1 1];
    end
end

% see if there are other arguments
for i=1:numel(varargin)
    if strcmp(lower(varargin{i}),'spherical')
        for j=1:numel(handles.im)
            sz = handles.im{j}.size;
            sp = handles.im{j}.spacing.*[180/pi 1 1]';
            or = handles.im{j}.origin.*[180/pi 1 1]';
            tmp = PatchType(sz,or,sp,eye(numel(sz)));
            tmp.data = handles.im{j}.data;
            handles.im{j} = tmp;
        end
    end
end


% % see if there is a file with the recently opened images
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentSavePath.txt']);
if fid<0
    % create the file
    fid = fopen([getuserdir '/.SimpleViewer_GUI_recentSavePath.txt'],'w');
    handles.recentSavePath = '/tmp/';
    fprintf(fid, handles.recentSavePath);
else
    % read the recent files
    files = (fread(fid,'*char'))';
    if numel(files)
        files = strsplit(files);
        handles.recentSavePath  = files{1};
    end
end
fclose(fid);

%%
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentFiles.txt']);
if fid<0
    % create the file
    fid = fopen([getuserdir '/.SimpleViewer_GUI_recentFiles.txt'],'w');
else
    % read the recent files
    files = (fread(fid,'*char'))';
    if numel(files)
        files = strsplit(files);
        set(handles.fileMenuOpenRecent_tag,'Enable','On');
        for i=1:numel(files)
            handles.fileMenuOpenRecent_tag_files(1) = uimenu(handles.fileMenuOpenRecent_tag,'Label',files{i},...
                'Callback',{@open_image_from_file, handles,files{i}});
            %associate a callback with this menu
        end
    end
end
fclose(fid);


guidata(hObject, handles);
updateData();

%-----------------------------------------------------------



% UIWAIT makes SimpleViewer_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SimpleViewer_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function fileMenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function fileMenuOpen_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpen_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename,pathname]= uigetfile('*.mhd','Open mhd image',[getuserdir '/data']);
handles.filename = [pathname filename];
handles.pathname = pathname;
guidata(hObject, handles);
if ~filename
    return;
end

% Add image to the recent images menu. First read the images already
% available, to make sure there is no more than 10
newfiles{1}=[pathname filename];
MAX_FILES=10;
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentFiles.txt'],'r');
if fid>0
    files = (fread(fid,'*char'))';
    if numel(files)
        % split by return
        files = strsplit(files);
        
        for i=1:min(numel(files),MAX_FILES-1)
            newfiles{i+1}=files{i};
        end
    end
    
    
end
fclose(fid);

% now write the file
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentFiles.txt'],'w');
if fid>0 && numel(newfiles)
    for i =1:numel(newfiles)
        fprintf(fid,'%s\n',newfiles{i});
    end
end
fclose(fid);

% enable the "open recent" tag

open_image_from_file(hObject, eventdata, handles, [pathname filename])

function open_image_from_file(hObject, eventdata, handles, filename)

set(handles.text_filename,'String',filename);
im = read_mhd(filename);

tmp = strfind(filename,filesep);
handles.pathname = filename(1:tmp(end));
handles.filename =filename;
guidata(hObject, handles);
if im.ndimensions ==2
    % convert the image to 3D to avoid problems
    M = eye(3);
    M(1:2,1:2) = im.orientation;
    im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
    im2.data = repmat(im.data,1,1,2);
    im = im2;
end

handles.filename = filename;
guidata(hObject, handles);

handles.im{1} = im;
handles.windowLimits{1}=[min(im.data(:)) max(im.data(:))];
handles.im{2} = [];
handles.n_images = [1 0];
guidata(hObject, handles);
updateData();






% --- Executes on slider movement.
function slider_time_Callback(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

t=floor(get(hObject,'Value'));
set(handles.frame_label,'String',[  num2str(t) '/' num2str(handles.im{1}.size(4))  ]);
handles.currentFrame = t;
guidata(hObject, handles);
for i=3:-1:1
    handles =bv_sliceUpdate_Fcn(0,i,hObject);
    guidata(hObject, handles);
end



% --- Executes during object creation, after setting all properties.
function slider_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
M = 2;
set(hObject,'Max',M,'Min',1,'Value',1,'SliderStep',[1/M 3/M]);
% slider_frame = uicontrol(fh,'Style','slider','Max',bms.size(4),'Min',1,'Value',1,'SliderStep',[1/bms.size(4) 10/bms.size(4)],'Position',[60 10 180 25],'callback',cb_frame);
% label_frame = uicontrol(fh,'Style','text','Position',[5 10 50 20],'String','Frame');
% label_frame2 = uicontrol(fh,'Style','text','Position',[250 10 40 20],'String',['1/' num2str(bmc.size(4))  ]);


% --- Executes on button press in pushbutton_pause.
function pushbutton_pause_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton_play,'Value',0)
guidata(hObject, handles);


% --- Executes on button press in pushbutton_play.
function pushbutton_play_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
gui_handles=guidata(hObject);
set(handles.pushbutton_play,'Enable','off');
while true % this is broken by the pause button only
    
    %for t=1:handles.im{1}.size(4)
    gui_handles=guidata(hObject);
    pause(0.05);
    
    if handles.pushbutton_play.Value == 0
        set(handles.pushbutton_play,'Value',1);
        guidata(hObject,gui_handles)
        break;
    end
    t = mod(gui_handles.currentFrame, gui_handles.im{1}.size(4))+1;
    set(gui_handles.slider_time,'Value',t);
    set(gui_handles.frame_label,'String',[  num2str(t) '/' num2str(gui_handles.im{1}.size(4))  ]);
    gui_handles.currentFrame = t;
    guidata(hObject, gui_handles);
    for i=3:-1:1
        gui_handles =bv_sliceUpdate_Fcn(0,i,hObject);
        guidata(hObject, gui_handles);
    end
    
end
set(gui_handles.pushbutton_play,'Enable','on');
guidata(hObject, gui_handles);


% --------------------------------------------------------------------
function fileMenuOpenOverly_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpenOverly_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~handles.n_images(1)
    disp('No image data!')
    return;
end
[filename,pathname]= uigetfile('*.mhd','Open mhd overly image');
if ~filename
    return;
end
im = read_mhd([pathname filename]);
if (handles.im{1}.ndimensions==im.ndimensions+1)
    M2 = eye(handles.im{1}.ndimensions);
    M2(1:end-1,1:end-1)=im.orientation;
    im2 = ImageType([im.size ;2], [im.origin ; -0.5], [im.spacing ; 1], M2) ;
    im2.data = repmat(im.data,1,1,2);
    im = im2;
end
im = resampleImage(im,handles.im{1},'in_gui');
handles.im{2} = im;
handles.windowLimits{2}=[-max(abs(im.data(:))) max(abs(im.data(:)))];
set(handles.checkbox_overlay,'Enable','on') ; %set to off
val = get(handles.checkbox_overlay,'Max');
set(handles.checkbox_overlay,'Value',val);  % uncheck
guidata(hObject, handles);
checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);
%updateData();


function updateData()
% this function is to be called when there are changes in input data. It
% should reload the views, adjust the slider, etc. It should first of all
% clean up all elements in the axis.

handles = guidata(gcf);

if ~handles.n_images(1)
    disp('No image data!')
    return;
end

for i=1:3
    all_objects = get(handles.axis_h(i),'Children');
    delete(all_objects );% if there is image data, remove it
end

if handles.enable3D
    all_objects = get(handles.axis_h(4),'Children');
    delete(all_objects );% if there is image data, remove it
end

guidata(gcf, handles);
bounds = handles.im{1}.GetBounds();
if numel(bounds)<6
    bounds(5:6)=[-0.5 0.5]';
end
axis_radius = norm(bounds([2 4 6])-bounds([1 3 5]))/6;

if numel(handles.im{1}.size)==4
    % 4D data
    set(handles.slider_time,'Enable','on');
    set(handles.uipanel_temporal,'Visible','on');
    set(handles.slider_time,'Max',handles.im{1}.size(4),'Min',1,'Value',1,'SliderStep',[1/(handles.im{1}.size(4)-1) 3/handles.im{1}.size(4)]);
else
    set(handles.uipanel_temporal,'Visible','off');
    set(handles.slider_time,'Enable','off');
end
xmin=bounds(1);
xmax=bounds(2);
ymin=bounds(3);
ymax=bounds(4);
zmin=bounds(5);
zmax=bounds(6);
im_centroid =[(xmax+xmin)/2 (ymax+ymin)/2 (zmax+zmin)/2]';
handles.centroid = im_centroid;
for i=1:3
    handles.image_centre{i} =im_centroid;
    guidata(gcf, handles);
end

cidx = [3 3 2];
sp_l = cell(3,1); % dragable lines
sp_line = cell(3,1); % non dragable lines
for i=1:3
    sp_l{i,1} = imline(handles.axis_h(i),[-axis_radius*handles.factor 0; 0+axis_radius 0]);
    set(gcf,'CurrentAxes',handles.axis_h(i));
    sp_line{i,1} = line([ 0 0], [-axis_radius*handles.factor  +axis_radius],'Color',handles.colors(cidx(i),:));
    axis equal;
end


handles.axis_radius=axis_radius;
handles.sp_l = sp_l;
handles.sp_line = sp_line;
%handles.sp_p = sp_p;
handles.handle_3D_slice=[0 0 0];
guidata(gcf, handles);

% update callbacks
sp_sliceUpdate=cell(3,3);
for i=1:3
    handles = bv_sliceUpdate_Fcn(0,i,handles.figure1);
    guidata(gcf, handles);
    %sp_sliceUpdate{i} = @(x)bv_sliceUpdate_Fcn(x,i,gcf);
end

setColor(sp_l{1},handles.colors(2,:));
sp1_cf = @(x)bv_axis_constrainFcn(x,1);
setPositionConstraintFcn(sp_l{1},sp1_cf);
changesp1_updatesp2 = addNewPositionCallback(sp_l{1},sp_sliceUpdate{2});
changesp1_updatesp3 = addNewPositionCallback(sp_l{1},sp_sliceUpdate{3});


%% Subplot (2,1)
% callback_point_handles{2} = addNewPositionCallback(sp_p{2},sp_lineUpdate{2});

setColor(sp_l{2},handles.colors(1,:));
sp2_cf = @(x)bv_axis_constrainFcn(x,2);
setPositionConstraintFcn(sp_l{2},sp2_cf);
%changesp2_updatesp1 = addNewPositionCallback(sp_l{2},sp_sliceUpdate{1});
%changesp2_updatesp3 = addNewPositionCallback(sp_l{2},sp_sliceUpdate{3});

%% Subplot (2,2)
%  callback_point_handles{3} = addNewPositionCallback(sp_p{3},sp_lineUpdate{3});

setColor(sp_l{3},handles.colors(1,:));
sp3_cf = @(x)bv_axis_constrainFcn(x,3);
setPositionConstraintFcn(sp_l{3},sp3_cf);
%changesp3_updatesp1 = addNewPositionCallback(sp_l{3},sp_sliceUpdate{1});
%changesp3_updatesp2 = addNewPositionCallback(sp_l{3},sp_sliceUpdate{2});

if handles.im{1}.size(end)<=2
    handles.axes1.Position = [0.2    0.1    0.8 0.9];
    handles.axes2.Position = [0.2    0.0    0.01    0.01];
    handles.axes3.Position = [0.2    0.0    0.01    0.01];
    handles.axes4.Position = [0.2    0.0    0.01    0.01];
    handles.checkbox9.Value=1;
    
    
else
    handles.axes1.Position = [0.1897    0.5609    0.3851    0.4370];
    handles.axes2.Position = [0.1897    0.0957    0.3851    0.4348];
    handles.axes3.Position = [0.6015    0.5609    0.3851    0.4370];
    handles.axes4.Position = [0.7020    0.0962    0.2844    0.3217];
    handles.checkbox9.Value=0;
    
end

guidata(gcf, handles);
checkbox9_Callback(handles.checkbox9, [],handles);


% --------------------------------------------------------------------
function fileMenuRemoveOverly_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuRemoveOverly_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~handles.n_images(2)
    disp('No overly data!')
    return;
end
handles.im{2} = [];
handles.windowLimits{2}=[0 255];
handles.n_images = [handles.n_images(1)  0];
guidata(hObject, handles);
val = get(handles.checkbox_overlay,'Min');
set(handles.checkbox_overlay,'Value',val);  % uncheck
set(handles.checkbox_overlay,'Enable','off') ; %set to off
updateData();


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

active = get(hObject,'Value');
if active
    handles.enable3D = true;
    guidata(hObject, handles);
    % update visualisation
    for i=3:-1:1
        handles =bv_sliceUpdate_Fcn(0,i,hObject);
        guidata(hObject, handles);
    end
else
    set(gcf,'CurrentAxes',handles.axis_h(4));
    % Something wrong here. These are not the appropriate handles
    for i=3:-1:1
        delete(handles.handle_3D_slice(i));
        handles.handle_3D_slice(i)=0;
    end
    handles.enable3D = false;
end
guidata(hObject, handles);





% --- Executes on button press in checkbox_overlay.
function checkbox_overlay_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_overlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_overlay
OnVal = get(hObject,'Max');
val = get(hObject,'Value');
if (val==OnVal)
    handles.n_images = [handles.n_images(1)  1];
else
    handles.n_images = [handles.n_images(1)  0];
end
guidata(hObject, handles);
% update visualisation
for i=3:-1:1
    handles =bv_sliceUpdate_Fcn(0,i,hObject);
    guidata(hObject, handles);
end



% --------------------------------------------------------------------
function about_menu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to about_menu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = msgbox('SimpleViewer4D (c) Alberto Gomez 2014 - Kings College London. alberto.gomez@kcl.ac.uk','About SimpleViewer4D');


% --------------------------------------------------------------------
function fileMenuLocal_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpenLocal_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ims = uigetvariables({'Please select an image from the workspace','Please select an overlay from the workspace'}, 'InputTypes',{'ImageType','ImageType'});
figure(handles.figure1);
%tvar = uigetvariables({'Pick a number:'}, ...         'InputType',{'string'});

if numel(ims{1})
    handles.im{1} = ims{1};
    handles.windowLimits{1}=[min(ims{1}.data(:)) max(ims{1}.data(:))];
    handles.im{2} = [];
    handles.n_images = [1 0];
    guidata(hObject, handles);
end

if numel(ims{2})
    % add an overlay
    if ~handles.n_images(1)
        disp('No image data! Cannot add overlay')
        return;
    end
    
    im = resampleImage(ims{2},handles.im{1},'in_gui');
    handles.im{2} = im;
    handles.windowLimits{2}=[-max(abs(im.data(:))) max(abs(im.data(:)))];
    set(handles.checkbox_overlay,'Enable','on') ; %set to off
    val = get(handles.checkbox_overlay,'Max');
    set(handles.checkbox_overlay,'Value',val);  % uncheck
    guidata(hObject, handles);
    checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);
end
updateData();


% --------------------------------------------------------------------
function toolMenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to toolMenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolMenu_rreg_Callback(hObject, eventdata, handles)
% hObject    handle to toolMenu_rreg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if (numel(handles.im{2}))
    rreg_dialog(hObject);
else
    h = msgbox('ERROR: you need an image and an overlay for registration!','ERROR');
end

% --------------------------------------------------------------------
function toolMenu_crop_Callback(hObject, eventdata, handles,cropType,maskType)
% hObject    handle to toolMenu_crop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.im)>1 && numel(handles.im{2})
    fileMenuRemoveOverly_tag_Callback(hObject, eventdata, handles);
end

if numel(handles.im{1}) %   && (numel(handles.im)==1 || numel(handles.im{2})==0)
    pts = impoly(handles.axes1);
    mask = zeros(handles.im{1}.size(1:2)');
    tmp = fliplr(createMask(pts)');
    clear pts;
    mask(1:size(tmp,1),1:size(tmp,2)) = tmp;
    maskImage = ImageType(handles.im{1});
    if handles.im{1}.size(end)==2
        % it is really a 2D image
        maskImage.data = repmat(double(mask),1,1,2);
    else
        h = msgbox('ERROR: Cropping is only implemented for 2D images!','ERROR');
        return;
    end
    maskImage = resampleImage(maskImage,handles.im{1},'in_gui');
    if strcmp(cropType,'square')
        bounds = maskImage.GetBounds(0);
        points = maskImage.GetPosition();
        in = find(points(1,:)>=bounds(1) & points(1,:)<=bounds(2) & points(2,:)>=bounds(3) & points(2,:)<=bounds(4));
        maskImage.data(in) = 1;
    end
    
    handles.im{2} = maskImage;
    if strcmp(lower(maskType),'mask')
        handles.im{2}.data(maskImage.data>0) = 1;
    else
        handles.im{2}.data(maskImage.data>0) = handles.im{1}.data(maskImage.data>0);
    end
    
    handles.windowLimits{2}=[-max(abs(maskImage.data(:))) max(abs(maskImage.data(:)))];
    set(handles.checkbox_overlay,'Enable','on') ; %set to off
    val = get(handles.checkbox_overlay,'Max');
    set(handles.checkbox_overlay,'Value',val);  % uncheck
    handles.n_images = [1 1];
    guidata(hObject, handles);
    updateData();
    % save to file
    %filename = handles.filename;
    %out = strsplit(filename,'/');
    %outstringprefix = out{end}(1:end-4);
    %outstringprefix_name = [outstringprefix '_annotationPolygon' ];
else
    h = msgbox('ERROR: you need an image and no overlauy!','ERROR');
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% do something


% --- Executes on button press in pushbutton_previous.
function pushbutton_previous_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.backgroundImageFiles)>0
    handles.currentImageFile = handles.currentImageFile-1;
    if handles.currentImageFile< 1
        handles.currentImageFile=numel(handles.backgroundImageFiles);
    end
    
    im = read_mhd(handles.backgroundImageFiles{handles.currentImageFile});
    if im.ndimensions ==2
        % convert the image to 3D to avoid problems
        M = eye(3);
        M(1:2,1:2) = im.orientation;
        im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
        im2.data = repmat(im.data,1,1,2);
        im = im2;
    end
    handles.filename = handles.backgroundImageFiles{handles.currentImageFile};
    guidata(hObject, handles);
    handles.im{1} = im;
    handles.windowLimits{1}=[min(im.data(:)) max(im.data(:))];
    
    if numel(handles.maskImageFiles)>0
        im = read_mhd(handles.maskImageFiles{handles.currentImageFile});
        
        if im.ndimensions ==2
            M(1:2,1:2) = im.orientation;
            im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
            im2.data = repmat(im.data,1,1,2);
            im = im2;
        end
        %handles.windowLimits{2}=[min(im.data(:)) max(im.data(:))];
        guidata(hObject, handles);
        handles.im{2} = PatchType(im);
        handles.im{2}.data = im.data;
    end
    guidata(hObject, handles);
    updateData();
    
    
else
    filter = get(handles.edit1,'String');
    if ~isfield(handles, 'pathname')
        return
    end
    list = dir([handles.pathname filter]);
    
    if numel(list)==0
        return;
    end
    
    if get(handles.checkbox7,'Value')
        % sort by time in name
        numbers = zeros(numel(list), 1);
        for i=1:numel(list)
            %names = strsplit(list(i).name, '3D_t');
            %names = strsplit(list(i).name, '2D_t');
            % check if files follow the standard ifind naming
            if numel(list(i).name) == 45
                names = strsplit(list(i).name, 'D_');
                number = names{2}(1:14);
                if number(1)=='t'
                    numbers(i) = str2num(number(2:end));
                else
                    numbers(i) = str2num(number(1:end-1));
                end
            else
                % not ifind so just go to the next alphabetically
                % sort by time
                [~,idx] = sort([list.datenum]);
                data = list(idx);
            end
        end
        [~,idx] = sort(numbers);
        data = list(idx);
    else
        % sort by time
        [~,idx] = sort([list.datenum]);
        data = list(idx);
    end
    
    for i=1:length(data)
        is = strcmp([ handles.pathname data(i).name] ,handles.filename);
        if is
            break;
        end
    end
    
    handles.current_file = i;
    guidata(hObject, handles);
    
    previous_file = i-1;
    if previous_file<1
        previous_file= length(data);
    end
    
    open_image_from_file(hObject, eventdata, handles, [ handles.pathname data(previous_file).name] );
end



% --- Executes on button press in pushbutton_next.
function pushbutton_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.backgroundImageFiles)>0
    handles.currentImageFile = handles.currentImageFile+1;
    if handles.currentImageFile>numel(handles.backgroundImageFiles)
        handles.currentImageFile=1;
    end
    
    im = read_mhd(handles.backgroundImageFiles{handles.currentImageFile});
    if im.ndimensions ==2
        % convert the image to 3D to avoid problems
        M = eye(3);
        M(1:2,1:2) = im.orientation;
        im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
        im2.data = repmat(im.data,1,1,2);
        im = im2;
    end
    handles.filename = handles.backgroundImageFiles{handles.currentImageFile};
    guidata(hObject, handles);
    handles.im{1} = im;
    handles.windowLimits{1}=[min(im.data(:)) max(im.data(:))];
    
    if numel(handles.maskImageFiles)>0
        im = read_mhd(handles.maskImageFiles{handles.currentImageFile});
        
        if im.ndimensions ==2
            M(1:2,1:2) = im.orientation;
            im2 = PatchType([im.size; 2], [im.origin ; -0.5], [im.spacing ; 1], M);
            im2.data = repmat(im.data,1,1,2);
            im = im2;
        end
        %handles.windowLimits{2}=[min(im.data(:)) max(im.data(:))];
        guidata(hObject, handles);
        handles.im{2} = PatchType(im);
        handles.im{2}.data = im.data;
    end
    guidata(hObject, handles);
    updateData();
else
    if ~isfield(handles, 'pathname')
        return
    end
    filter = get(handles.edit1,'String');
    list = dir([handles.pathname filter]);
    
    if numel(list)==0
        return;
    end
    
    if get(handles.checkbox7,'Value')
        % sort by time in name
        numbers = zeros(numel(list), 1);
        for i=1:numel(list)
            %names = strsplit(list(i).name, '3D_t');
            %names = strsplit(list(i).name, '2D_t');
            % check if files follow the standard ifind naming
            if numel(list(i).name) == 45
                names = strsplit(list(i).name, 'D_');
                number = names{2}(1:14);
                if number(1)=='t'
                    numbers(i) = str2num(number(2:end));
                else
                    numbers(i) = str2num(number(1:end-1));
                end
            else
                % not ifind so just go to the next alphabetically
                % sort by time
                [~,idx] = sort([list.datenum]);
                data = list(idx);
            end
        end
        [~,idx] = sort(numbers);
        data = list(idx);
    else
        % sort by time
        [~,idx] = sort([list.datenum]);
        data = list(idx);
    end
    
    
    for i=1:length(data)
        is = strcmp([ handles.pathname data(i).name] ,handles.filename);
        if is
            break;
        end
    end
    
    handles.current_file = i;
    guidata(hObject, handles);
    
    next_file = i+1;
    if next_file>length(data)
        next_file = 1;
    end
    
    open_image_from_file(hObject, eventdata, handles, [ handles.pathname data(next_file).name] );
end


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7

% --- Executes on button press in pushbutton_delete_file.
function pushbutton_delete_file_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_delete_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% find next to display afterwards

file_to_delete = handles.filename;
filter = get(handles.edit1,'String');
list = dir([handles.pathname filter]);

if get(handles.checkbox7,'Value')
    % sort by time in name
    numbers = zeros(numel(list), 1);
    for i=1:numel(list)
        %names = strsplit(list(i).name, '3D_t');
        %names = strsplit(list(i).name, '2D_t');
        names = strsplit(list(i).name, 'D_t');
        number = names{2}(1:13);
        numbers(i) = str2num(number);
    end
    [~,idx] = sort(numbers);
    data = list(idx);
else
    % sort by time
    [~,idx] = sort([list.datenum]);
    data = list(idx);
end

for i=1:length(data)
    is = strcmp([ handles.pathname data(i).name] ,handles.filename);
    if is
        break;
    end
end


% Construct a questdlg with are you sure? question
choice = questdlg(['WARNING: Do you want to delete ' file_to_delete '? This operation cannot be undone.'], ...
    'Delete file', ...
    'Yes','No','No');
% Handle response
switch choice
    case 'Yes'
        disp([' Deleting ' file_to_delete])
    case 'No'
        return
end

% switch to next image

handles.current_file = i;
guidata(hObject, handles);

next_file = i+1;
if next_file>length(data)
    next_file = 1;
end
open_image_from_file(hObject, eventdata, handles, [ handles.pathname data(next_file).name] );

% delete file
idx_separator = strfind(file_to_delete,'.');
system(['rm ' file_to_delete(1:idx_separator) '*']);
disp([' File ' file_to_delete ' deleted'])


% --------------------------------------------------------------------
function fileMenuOpenRecent_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpenRecent_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_previous10.
function pushbutton_previous10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_previous10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filter = get(handles.edit1,'String');
list = dir([handles.pathname filter]);

if get(handles.checkbox7,'Value')
    % sort by time in name
    numbers = zeros(numel(list), 1);
    for i=1:numel(list)
        names = strsplit(list(i).name, 'D_');
        number = names{2}(1:14);
        if number(1)=='t'
            numbers(i) = str2num(number(2:end));
        else
            numbers(i) = str2num(number(1:end-1));
        end
    end
    [~,idx] = sort(numbers);
    data = list(idx);
else
    % sort by time
    [~,idx] = sort([list.datenum]);
    data = list(idx);
end


for i=1:length(data)
    is = strcmp([ handles.pathname data(i).name] ,handles.filename);
    if is
        break;
    end
end

handles.current_file = i;
guidata(hObject, handles);

previous_file = i-10;
if previous_file<1
    previous_file= length(data)+previous_file;
end

open_image_from_file(hObject, eventdata, handles, [ handles.pathname data(previous_file).name] );


% --- Executes on button press in pushbutton_next10.
function pushbutton_next10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


filter = get(handles.edit1,'String');
list = dir([handles.pathname filter]);


if get(handles.checkbox7,'Value')
    % sort by time in name
    numbers = zeros(numel(list), 1);
    for i=1:numel(list)
        %names = strsplit(list(i).name, '3D_t');
        %names = strsplit(list(i).name, '2D_t');
        names = strsplit(list(i).name, 'D_');
        number = names{2}(1:14);
        if number(1)=='t'
            numbers(i) = str2num(number(2:end));
        else
            numbers(i) = str2num(number(1:end-1));
        end
    end
    [~,idx] = sort(numbers);
    data = list(idx);
else
    % sort by time
    [~,idx] = sort([list.datenum]);
    data = list(idx);
end

for i=1:length(data)
    is = strcmp([ handles.pathname data(i).name] ,handles.filename);
    if is
        break;
    end
end

handles.current_file = i;
guidata(hObject, handles);

next_file = i+10;
if next_file>length(data)
    next_file = 1+next_file-length(data);
end

open_image_from_file(hObject, eventdata, handles, [ handles.pathname data(next_file).name] );



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_opacity_Callback(hObject, eventdata, handles)
% hObject    handle to slider_opacity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.opacity = get(hObject,'Value');
guidata(hObject, handles);
updateData();



% --- Executes during object creation, after setting all properties.
function slider_opacity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_opacity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in popupmenu_colormap.
function popupmenu_colormap_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_colormap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_colormap contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_colormap
contents = cellstr(get(hObject,'String'));
val = contents{get(hObject,'Value')};

handles.windowLimits{2}=[min(handles.im{2}.data(:)) max(handles.im{2}.data(:))];

if strcmp(lower(val),'dopplercolors')
    handles.colormap{2}=dopplerColors;
    handles.windowLimits{2}=[-max(abs(handles.im{2}.data(:))) max(abs(handles.im{2}.data(:)))];
elseif strcmp(lower(val),'gray')
    handles.colormap{2}=gray;
elseif strcmp(lower(val) ,'green')
    handles.colormap{2}=summer;
elseif strcmp(lower(val) ,'jet')
    handles.colormap{2}=jet;
elseif strcmp(lower(val) ,'blue')
    handles.colormap{2}=winter;
end
guidata(hObject, handles);
updateData();



% --- Executes during object creation, after setting all properties.
function popupmenu_colormap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_colormap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_getAxes.
function pushbutton_getAxes_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_getAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


M = eye(4,4);
M(1:3,1:3) = handles.CurrentAxisMatrix;
M(1:3,4) = mean([handles.image_centre{1} handles.image_centre{2} handles.image_centre{3}],2);
M


% --- Executes on button press in checkbox9.
function checkbox9_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox9

dohide = get(hObject,'Value');
if dohide
    for i=1:3
        delete(handles.sp_l{i,1});
        delete(handles.sp_line{i,1})
    end
else
    cidx = [3 3 2];
    sp_l = cell(3,1); % dragable lines
    sp_line = cell(3,1); % non dragable lines
    for i=1:3
        if isvalid(handles.sp_l{i,1})
            delete(handles.sp_l{i,1});
        end
        handles.sp_l{i,1} = imline(handles.axis_h(i),[-handles.axis_radius*handles.factor 0; 0+handles.axis_radius 0]);
        % set(gcf,'CurrentAxes',handles.axis_h(i));
        % handles.sp_line{i,1} = line([ 0 0], [-handles.axis_radius*handles.factor  +handles.axis_radius],'Color',handles.colors(cidx(i),:));
        axis equal;
    end
    guidata(hObject, handles);
    
    % update callbacks
    sp_sliceUpdate=cell(3,3);
    for i=1:3
        handles_ =bv_sliceUpdate_Fcn(0,i,handles.figure1);
        guidata(gcf, handles_);
        sp_sliceUpdate{i} = @(x)bv_sliceUpdate_Fcn(x,i,gcf);
    end
    
    setColor(handles.sp_l{1},handles.colors(2,:));
    sp1_cf = @(x)bv_axis_constrainFcn(x,1);
    setPositionConstraintFcn(handles.sp_l{1},sp1_cf);
    changesp1_updatesp2 = addNewPositionCallback(handles.sp_l{1},sp_sliceUpdate{2});
    changesp1_updatesp3 = addNewPositionCallback(handles.sp_l{1},sp_sliceUpdate{3});
    
    
    %% Subplot (2,1)
    % callback_point_handles{2} = addNewPositionCallback(sp_p{2},sp_lineUpdate{2});
    
    setColor(handles.sp_l{2},handles.colors(1,:));
    sp2_cf = @(x)bv_axis_constrainFcn(x,2);
    setPositionConstraintFcn(handles.sp_l{2},sp2_cf);
    changesp2_updatesp1 = addNewPositionCallback(handles.sp_l{2},sp_sliceUpdate{1});
    changesp2_updatesp3 = addNewPositionCallback(handles.sp_l{2},sp_sliceUpdate{3});
    
    %% Subplot (2,2)
    %  callback_point_handles{3} = addNewPositionCallback(sp_p{3},sp_lineUpdate{3});
    
    setColor(handles.sp_l{3},handles.colors(1,:));
    sp3_cf = @(x)bv_axis_constrainFcn(x,3);
    setPositionConstraintFcn(handles.sp_l{3},sp3_cf);
    changesp3_updatesp1 = addNewPositionCallback(handles.sp_l{3},sp_sliceUpdate{1});
    changesp3_updatesp2 = addNewPositionCallback(handles.sp_l{3},sp_sliceUpdate{2});
    
    
end


% --------------------------------------------------------------------
function fileMenuSave2D_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2D_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filename = handles.filename;
out = strsplit(filename,'/');
outstringprefix = out{end}(1:end-4);

[file,path] = uiputfile(outstringprefix,'Save file prefix', [handles.recentSavePath  outstringprefix]);

fid = fopen([getuserdir '/.SimpleViewer_GUI_recentSavePath.txt'],'w');
if fid>0
    fprintf(fid, path,'w');
end
fclose(fid);

handles.recentSavePath = path;
guidata(hObject, handles);
for j = 1:3
    for i=1:2
        if numel(handles.im{i})>0
            save_view(hObject, eventdata, handles, j, i, file, path)
        end
    end
end

%write_ITKMatrix([ path file '_slicingMatrix.mat'],M2_3D);


% --------------------------------------------------------------------
function fileMenuSave2Db1_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Db1_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 1, 1, file, path)


% --------------------------------------------------------------------
function fileMenuSave2Do1_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Do1_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 1, 2, file, path)


% --------------------------------------------------------------------
function fileMenuSave2Do2_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Do2_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 2, 2, file, path)


% --------------------------------------------------------------------
function fileMenuSave2Do3_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Do3_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 3, 2, file, path)

% --------------------------------------------------------------------
function fileMenuSave2Db2_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Db2_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 2, 1, file, path)

% --------------------------------------------------------------------
function fileMenuSave2Db3_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuSave2Db3_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uiputfile('slice','Save file prefix');
save_view(hObject, eventdata, handles, 3, 1, file, path)


function save_view(hObject, eventdata, handles, v, layer, file, path)



j = v;
M2_3D = eye(4);
M2_3D(1:3,1:3)= handles.CurrentAxisMatrix/handles.Rot{j} * handles.Mslice{j};
M2_3D(1:3,4)=  handles.image_centre{j};
M1_3D = eye(4);
M1_3D(1:3,1:3)= handles.CurrentAxisMatrix * handles.Mslice{j};
M1_3D(1:3,4)=  handles.image_centre{j};
i=layer;
if handles.im{i}.ndimensions ==4
    im3D = handles.im{i}.extractFrame(handles.currentFrame);
else
    im3D = handles.im{i};
end
[sl3,slice] = resliceImage(im3D,'mat',M1_3D,'interpolation','NN');
% adjust for crossahair offset
xoff = handles.sp_l{j}.getPosition();
xoffvec = xoff(2,:)-xoff(1,:);
%offset = xoff(1,:)+xoffvec*handles.factor;
offset = (xoff(1,:)+xoffvec/norm(xoffvec)*handles.factor*handles.axis_radius)';
%vertical offset
%yoff = [get(handles.sp_line{j,1},'XData')'  get(handles.sp_line{j,1},'YData')'];
%yoffvec = yoff(2,:)-yoff(1,:);
%offsety = yoff(1,:)+yoffvec/norm(yoffvec)*handles.factor*handles.axis_radius;
slice.origin = slice.origin - offset;
write_mhd([ path file '_unwarped_layer' num2str(i) '_slice' num2str(j) '.mhd'], slice);
[sl3,slice] = resliceImage(im3D,'mat',M2_3D,'interpolation','NN');
write_mhd([ path file '_layer' num2str(i) '_slice' num2str(j) '.mhd'], slice);


% --- Executes on button press in pushbutton_addPolygon.
function pushbutton_addPolygon_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_addPolygon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pts = impoly(handles.axes1);
mask = zeros(handles.im{1}.size(1:2)');
tmp = fliplr(createMask(pts)');
mask(1:size(tmp,1),1:size(tmp,2)) = tmp;
maskImage = ImageType(handles.im{1}.size(1:2), handles.im{1}.origin(1:2), handles.im{1}.spacing(1:2), handles.im{1}.orientation(1:2,1:2));
maskImage.data = double(mask);

% save to file
filename = handles.filename;
out = strsplit(filename,'/');
outstringprefix = out{end}(1:end-4);
outstringprefix_name = [outstringprefix '_annotationPolygon' ];


if ~isfield(handles,'recentSavePath')
    handles.recentSavePath = getuserdir();
end
[file,path] = uiputfile(outstringprefix_name,'Save file prefix', [handles.recentSavePath  outstringprefix_name]);
if file==0
    delete(pts);
    return;
end
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentSavePath.txt'],'w');
if fid>0
    fprintf(fid, path,'w');
end
fclose(fid);

dlmwrite([path file '_points.txt'],pts.getPosition,'\t');
write_mhd([path file '_segmentation.mhd'],maskImage,'ElementType','int16');

handles.recentSavePath = path;
guidata(hObject, handles);
delete(pts);


% --- Executes on button press in pushbutton_addEllipse.
function pushbutton_addEllipse_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_addEllipse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


pts = imellipse(handles.axes1);
mask = zeros(handles.im{1}.size(1:2)');
tmp = fliplr(createMask(pts)');
mask(1:size(tmp,1),1:size(tmp,2)) = tmp;
maskImage = ImageType(handles.im{1}.size(1:2), handles.im{1}.origin(1:2), handles.im{1}.spacing(1:2), handles.im{1}.orientation(1:2,1:2));
maskImage.data = double(mask);

% save to file
filename = handles.filename;
out = strsplit(filename,'/');
outstringprefix = out{end}(1:end-4);
outstringprefix_name = [outstringprefix '_annotationEllipse' ];

if ~isfield(handles,'recentSavePath')
    handles.recentSavePath = getuserdir();
end
[file,path] = uiputfile(outstringprefix_name,'Save file prefix', [handles.recentSavePath  outstringprefix_name]);
if file==0
    delete(pts);
    return;
end
fid = fopen([getuserdir '/.SimpleViewer_GUI_recentSavePath.txt'],'w');
if fid>0
    fprintf(fid, path,'w');
end
fclose(fid);

dlmwrite([path file '_points.txt'],pts.getPosition,'\t');
write_mhd([path file '_segmentation.mhd'],maskImage,'ElementType','int16');

handles.recentSavePath = path;
guidata(hObject, handles);
delete(pts);


% --- Executes on button press in pushbutton_SNR.
function pushbutton_SNR_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_SNR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%pts1 = impoly(handles.axes1);
%mask1 = zeros(handles.im{1}.size(1:2)');
%tmp = fliplr(createMask(pts1)');
%mask1(1:size(tmp,1),1:size(tmp,2)) = tmp;
%maskImage1 = ImageType(handles.im{1}.size(1:2), handles.im{1}.origin(1:2), handles.im{1}.spacing(1:2), handles.im{1}.orientation(1:2,1:2));
%maskImage1.data = double(mask1);

pts2 = impoly(handles.axes1);
mask2 = zeros(handles.im{1}.size(1:2)');
tmp = fliplr(createMask(pts2)');
mask2(1:size(tmp,1),1:size(tmp,2)) = tmp;
maskImage2 = ImageType(handles.im{1}.size(1:2), handles.im{1}.origin(1:2), handles.im{1}.spacing(1:2), handles.im{1}.orientation(1:2,1:2));
maskImage2.data = double(mask2);

% Compute

%data_background = handles.im{1}.data(:,:,1).*mask1.data;
data_foreground = handles.im{1}.data(:,:,1).*maskImage2.data;

SNR = 10*log10(mean(data_foreground(:))/std(data_foreground(:)));
disp(['SNR: ' num2str(SNR) 'dB'])




guidata(hObject, handles);
delete(pts2);


% --------------------------------------------------------------------
function tag_save_background_image_Callback(hObject, eventdata, handles)
% hObject    handle to tag_save_background_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.im{1})
    outstringprefix =  'background.mhd';
    [file,path] = uiputfile(outstringprefix,'Save file ', [handles.recentSavePath  outstringprefix]);
    
    if file==0
        return;
    end
    
    if handles.im{1}.ndimensions==3 && handles.im{1}.size(3)==2
        % this is actually a 2D image
        im =  handles.im{1}.extractFrame(1);
    else
        im =  handles.im{1};
    end
    write_mhd([path file],im);
    
    handles.recentSavePath = path;
    guidata(hObject, handles);
    
    
end


% --------------------------------------------------------------------
function tag_save_foreground_image_Callback(hObject, eventdata, handles)
% hObject    handle to tag_save_foreground_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.im)>1 &&  numel(handles.im{2})
    outstringprefix =  'foreground.mhd';
    [file,path] = uiputfile(outstringprefix,'Save file ', [handles.recentSavePath  outstringprefix]);
    
    if file==0
        return;
    end
    
    if handles.im{2}.ndimensions==3 && handles.im{2}.size(3)==2
        % this is actually a 2D image
        im =  handles.im{2}.extractFrame(1);
    else
        im =  handles.im{2};
    end
    write_mhd([path file],im);
    
    handles.recentSavePath = path;
    guidata(hObject, handles);
    
    
end

function userDir = getuserdir
%GETUSERDIR   return the user home directory.
%   USERDIR = GETUSERDIR returns the user home directory using the registry
%   on windows systems and using Java on non windows systems as a string
%
%   Example:
%      getuserdir() returns on windows
%           C:\Documents and Settings\MyName\Eigene Dateien

if ispc
    userDir = winqueryreg('HKEY_CURRENT_USER',...
        ['Software\Microsoft\Windows\CurrentVersion\' ...
        'Explorer\Shell Folders'],'Personal');
else
    userDir = char(java.lang.System.getProperty('user.home'));
end